var tempCorrect = [
    {"number" : 1 , "answer": "<ul><li>Cokelat.</li><li>Hitam.</li><li>Abu-abu.</li></ul>"},
    {"number" : 2 , "answer": "Warna bulu mereka berbeda."},
    {"number" : 3 , "answer": "Perkotaan.Bukti kalimat yang tepat (menuliskan minimal satu bukti):<br><ul><li>	Apakah sekarang di Jakarta ini masih ada sumur seperti di rumah kakeknya itu?</li><li>Langit ibu kota sekarang tampak cerah.</li></ul>"},
    {"number" : 4 , "answer": "<ul><li>Bertubuh kecil</li><li>Hidup berkelompok</li></ul>"},
    {"number" : 5 , "answer": "Mereka berselisih paham tentang posisi sumur yang akan mereka buat"},
    {"number" : 6 , "answer": "<img src=\"data/images/01/denahb.jpg\" />"},
    {"number" : 7 , "answer": "Teguh pendirian"},
    {"number" : 8 , "answer": "Memilih <strong>“Setuju”</strong> ATAU <strong>“Tidak Setuju”</strong> disertai alasan yang tepat. Alasan yang tepat untuk <strong>“Setuju”</strong>:<br><ul><li>Jika kita yakin pilihan kita lebih tepat karena alasan yang kuat dan masuk akal maka kita berhak mempertahankannya.</li><br>Alasan yang tepat untuk <strong>“Tidak Setuju”</strong>:<br><li>	Ajaran leluhur harus dipegang teguh.</li><li>	Kita tetap harus menghormati ajaran leluhur</li></ul>"},
    {"number" : 9 , "answer": "Pagi setelah hujan semalam."},
    {"number" : 10, "answer": "Soekram, Minuk, dan Esa.Kehidupan Soekram saat ini (menjawab minimal satu):<br><ul><li>Tidak selalu bahagia.</li><li>Kadang-kadang sedih</li></ul>"},
    {"number" : 11, "answer": "Ciri-ciri pancaroba (Menuliskan minimal 3):<ul><li>Daun-daun belum menjadi kecokelat cokelatan.</li><li> Daun-daun belum gugur.</li><li> Pohon-pohon belum gundul.</li><li>Burung-burung kecil kadang-kadang masih harus mencari tempat berteduh.</li><li> Cuaca yang terik bisa mendadak gelap</li><li>Muncul angin bertiup entah ke mana atau dari mana.</li><li> Muncul angin yang berputar-putar.</li></ul>"},
    {"number" : 12, "answer": "Penyabar<br>Dan Minuk harus membujuknya dengan sabar, kalau perlu dengan permen cokelat."},
    {"number" : 13, "answer": "Positif.<br> Alasan yang dapat tepat:<ul><li>Penulis menggunakan kata-kata yang indah dan positif dalam menggambarkan pancaroba.</li><br>Bukti kalimat dalam teks:<br>-	Dilihatnya langit ibu kota sekarang tampak cerah.<li>Penulis punya pandangan yang berbeda dengan orang-orang kebanyakan. <br>Bukti kalimat dalam teks:<br>-	Manusia tidak memahaminya. Justru mereka menyebutnya pancaroba yang dianggap sebagai sumber berbagai penyakit terutama bagi anak-anak. "},
    {"number" : 14, "answer": "Karena untuk menunjukkan ketidaksetujuan penulis terhadap anggapan manusia secara umum mengenai pergantian musim."},
    {"number" : 15, "answer": "<ul><li>3</li><li>5</li><li>4</li><li>1</li><li>2</li></ul>"},
    {"number" : 16, "answer": "Agar Soekram memiliki banyak baju ganti"},
    {"number" : 17, "answer": "Tujuan penulis yaitu untuk menggambarkan status ekonomi keluarga Soekram."},
    {"number" : 18, "answer": "Maksud penulis (Menjawab minimal satu):<ul><li>Di bulan Mei hujan dan kemarau akan terjadi.</li><li>Pancaroba masih terjadi di bulan Mei ini. </li>"}

];
