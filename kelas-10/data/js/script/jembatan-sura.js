var tempCorrect = [

    {"number" : 1 , "answer": "Karena nama ini merupakan julukan sekolah mereka."},

    {"number" : 2 , "answer": "<ul><li>Ada rumor tentang hantu di Jembata Sura.</li><li>Sering ditemukan mayat di dekat Jembatan Sura.</li></ul>"},

    {"number" : 3 , "answer": "Para polisi dan warga sedang berkerumun di sungai karena sesosok mayat ditemukan di sekitar Jembatan Sura."},

    {"number" : 4 , "answer": "Sekolah mereka sudah berusia lebih dari 30 tahun."},

    {"number" : 5 , "answer": "Variasi jawaban<ul><li>Karakter Pak Hardiman sulit ditebak.</li><li>Pak Hardiman mungkin tidak seramah orang lain pada umumnya.</li><li>Pak Hardiman mungkin memiliki penampilan fisik yang unik.</li></ul>"},

    {"number" : 6 , "answer": "marah"},

    {"number" : 7 , "answer": "Mereka mendatangi Jembatan Sura pada pagi hari untuk mencari tahu yang terjadi di Jembatan Sura."},

    {"number" : 8 , "answer": "Membersihkan jembatan dan sungai dari sampah-sampah / memungut sampah-sampah"},

    {"number" : 9 , "answer": "<ul><li>Ya. Dia pernah melihat sosok berbaju putih.</li><li>Tidak. Dia berkata seperti itu untuk menakut-nakuti Pasukan Merdeka saja.</li></ul>"},

    {"number" : 10, "answer": "<ul><li>Membawa kayu</li><li>Berbaju putih</li></ul>"},

    {"number" : 11, "answer": "tampak khawatir"},

    {"number" : 12, "answer": "<ul><li>Bukti-bukti yang dibutuhkan sudah cukup</li><li>Sulitnya menemui Pak Hardiman</li></ul>"},

    {"number" : 13, "answer": "Mereka ingin memastikan, siapa sosok yang mereka temui pagi-pagi sekali itu"},

    {"number" : 14, "answer": "Rifki, dia menuduh Pak Hardiman saat baru saja tiba di rumahnya."},

    {"number" : 15, "answer": "pemaaf dan penyabar"},

    {"number" : 16, "answer": "Ya, karena inisial namanya ada di sana, diperkuat dengan tulisan yang sama di pagar rumah Pak Hardiman dan penjelasan Pak Hardiman.<br><br>Tidak, selain sobekan kaos perlu bukti lain dari saksi yang menyatakan bahwa siapa pemilik robekan kaos itu dan kaitannya dengan Jembatan Sura."},

    {"number" : 17, "answer": "<ul><li>1</li><li>3</li><li>4</li><li>5</li><li>2</li></ul>"},

    {"number" : 18, "answer": "<ul><li>5 - Pak Hardiman memberikan informasi tentang identitas pemilik robekan kain.</li><li>1 - Kelompok Merdeka memutuskan mencari tahu melalui Pak Susanto.</li><li>3 - Pak Tugimin membenarkan adanya rumor tentang hantu Pak Sura.</li><li>4 - Dimas berhasil merobek kaos sosok berbaju putih setelah mengejarnya.</li><li>6 - Pak Tugimin  mengakui perannya dalam kasus ditemukannya mayat di Jembatan Sura.</li><li>2 - Kelompok Merdeka gagal menemui Pak Hardiman.</li></ul>"},

    {"number" : 19, "answer": "Tidak. Meskipun bertujuan baik, menakut-nakuti orang yang dapat mencelakakan orang adalah tindakan kriminal."},

    {"number" : 20, "answer": "<ul><li>Sebelum mempercayai sesuatu, kita harus mencari informasi dan membuktikannya.</li><li>Niat baik harus dilaksanakan dengan cara-cara yang baik juga.</li></ul>"}

];

