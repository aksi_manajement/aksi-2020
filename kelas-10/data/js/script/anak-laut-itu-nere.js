var tempCorrect = [
    {"number" : 1 , "answer": "Merembet"},
    {"number" : 2 , "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li>Senang mandi pagi di laut</li><li>Karena berangkat ke sekolah</li></ul>"},
    {"number" : 3 , "answer": "Nare yakin dengan meraih ilmu dan memiliki banyak teman dapat meningkatkan kesejahteraan ekonomi."},
    {"number" : 4 , "answer": "Sebagian besar hidup mereka dihabiskan di laut."},
    {"number" : 5 , "answer": "Suku Bajo hidup nomaden dengan berpindah-pindah tempat tinggal namun tetap di atas laut."},
    {"number" : 6 , "answer": "Ia sering membaca buku di perpustakaan sekolah."},
    {"number" : 7 , "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li>Pemikiran Nare yang berbeda dengan penduduk suku Bajo pada umumnya.</li><li>Hanya Nare yang memikirkan pentingnya sekolah.</li><li>Keyakinan Nare tentang meraih ilmu/sekolah dapat meningkatkan perekonomian.</li></ul>"},
    {"number" : 8 , "answer": "Mereka merasa sekolah menjauhkan Nare dari tradisi nenek moyang"},
    {"number" : 9 , "answer": "Cuek"},
    {"number" : 10, "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li>Nare masih mendapat dukungan dari ibunya untuk bersekolah.</li><li>Ayah Nare masih membiarkannya untuk berangkat ke sekolah.</li><li>Keinginan Nare yang kuat untuk bersekolah.</li></ul>"},
    {"number" : 11, "answer": "Ayah Nare semakin lemah dalam melaut karena semakin tua. "},
    {"number" : 12, "answer": "<ul><li>Ayah Nare terpeleset saat melaut.</li><li>Ayah Nare ditertawakan oleh teman-temannya.</li><li>Majikan kapal merajuk pada Ayah Nare.</li><li>Wajah Ayah murung melihat perlakuan teman-temannya.</li></ul>"},
    {"number" : 13, "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li> Karena kakak ingin menunjukkan bahwa ia memiliki banyak uang.</li><li>Karena kakak ingin membelikan ayah sebuah kapal supaya bisa melaut sendiri.</li><li>Karena kakak tidak ingin ayah melaut bersama majikannya di kapal besar.</li></ul>"},
    {"number" : 14, "answer": "Nare ingin mengangkat harkat  keluarganya dan harkat suku Bajo."},
    {"number" : 15, "answer": "<ul><li>Mencari uang. </li><li>Membeli perahu untuk ayah.</li></ul>"},
    {"number" : 16, "answer": "Menjual ikan melalui toko daring"},
    {"number" : 17, "answer": "<ul><li>(2) Kakak Nare memotret ikan hasil tangkapan dan mengirimnya ke Nare. </li><li>(4) Konsumen memilih dan membayar ikan.</li><li>(1) Kakak Nare dan ayah Nare melaut.</li><li>(5) Kakak Nare dan ayah Nare tidak pergi ke tempat  pelelangan ikan.</li><li>(3) Nare mengunggah foto ikan disertai spesifikasi dan harga.</li></ul>"},
    {"number" : 18, "answer": "Saung baca dan aplikasi penjualan ikan."},
    {"number" : 19, "answer": "Senyum kepada Nare."},
    {"number" : 20, "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li> Nare akan mengikuti jejak ayahnya dan masyarakat suku Bajo pada umumnya, yaitu melaut.</li><li>Nare mungkin akan mengikuti jejak kakaknya untuk bekerja ke negeri jiran.</li><li>Tidak ada yang berubah, baik dengan kehidupan Nare, maupun masyarakat suku Bajo.</li><li>Anak suku Bajo tidak memiliki saung membaca.</li><li>Masyaralat suku Bajo akan tetap ke daratan untuk menjual hasil tangkapan mereka.</li></ul>"}


];
