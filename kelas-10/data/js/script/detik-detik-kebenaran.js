var tempCorrect = [
    {"number" : 1 , "answer": "gugup"},
    {"number" : 2 , "answer": "rendah diri"},
    {"number" : 3 , "answer": "<ol><li>Jantungku berdegup kencang.</li><li>Sahara pucat pasi.</li><li>Lintang diam misterius.</li></ol>"},
    {"number" : 4 , "answer": "Kemungkinan Jawaban: <ul><li>Menang lomba sangat berarti bagi sekolah kampung</li><li>	Dapat mengangkat nama sekolah kampung</li></ul>"},
    {"number" : 5 , "answer": "<ul><li>(5) Menjawab benar dapat nilai 100. </li><li>(1) Meletakkan tangan di atas tombol</li><li>(4) Menjawab setelah disebut nama regunya</li><li>(3) Memencet tombol sebelum menjawab</li><li>(2) Mendengarkan pertanyaan dari pembaca soal</li></ul>"},
    {"number" : 6 , "answer": "lima belas tahun"},
    {"number" : 7 , "answer": "Pembaca soal"},
    {"number" : 8 , "answer": "Cerdas."},
    {"number" : 9 , "answer": "Wajahnya mirip Benyamin S"},
    {"number" : 10, "answer": "<ul><li>(&#10003;)Wajah terpana</li><li>(&#10003;)Berkecil hati</li><li>(&#10003;)Membanting pensil</li></ul>"},
    {"number" : 11, "answer": "<ol><li>Menggunakan teknik <i>radiocarbon</i></li><li>Melepaskan energi sinar dalam suhu panas (<i>thermoluminescent dating</i>)</li></ol>"},
    {"number" : 12, "answer": "Lintang memencet bel sebelum soal selesai dibacakan. "},
    {"number" : 13, "answer": "Tidak.<br>Bukti Kalimat:<ul><li>Aku melihatnya bertepuk tak henti-henti, berteriak-teriak memberi semangat, tetapi wajahnya tak melihat ke arah kami.</li><li>Ia menoleh ke luar jendela.</li><li>Kiranya ia sedang memberi semangat kepada sekelompok anak perempuan yang sedang bermain kasti di halaman.</li></ul>"},
    {"number" : 14, "answer": "Jika benar menjawab pertanyaan dapat nilai seratus, salah menjawab pertanyaan nilai dikurangi seratus."},
    {"number" : 15, "answer": "Kuli kopra"},
    {"number" : 16, "answer": "<ul><li>miskin</li><li>kurang mampu</li><li>ekonomi kelas rendah</li></ul>"},
    {"number" : 17, "answer": "Sekolah PN tidak kebagian kesempatan menjawab benar"},
    {"number" : 18, "answer": "Peristiwa dalam lomba terus berulang."},	
    {"number" : 19, "answer": "Bangga."},	
    {"number" : 20, "answer": "Menjawab minimal satu dari jawaban berikut.<ol><li>Hilang harapan.</li><li>Merasa jerih payahnya sia-sia.</li></ol>"}


];
