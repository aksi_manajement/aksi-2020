var tempCorrect = [

    {"number" : 1 , "answer": "Kemungkinan Jawaban:<ul><li>Ia takut terkena <i>tesafo</i>.</li><li>Ia diingatkan oleh Lisa.</li></ul>"},


    {"number" : 2 , "answer": "Karena sosok Pak Guru Adam memiliki pengaruh tersendiri dalam pikiran Mesilina."},


    {"number" : 3 , "answer": "<ul><li>(&#10003;)kain</li><li>(&#10003;)emas</li><li>(&#10003;)babi</li><li>(&#10003;)uang</li></ul>"},


    {"number" : 4 , "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li>Karena <i>bowo</i> atau biaya yang akan dikeluarkan tidak terlalu besar.</li><li>Karena semakin tinggi <i>bosi</i> atau status sosial keluarga gadis itu, maka semakin tinggi <i>bowo</i> yang harus diberikan</li></ul>"},


    {"number" : 5 , "answer": "Karena berdasarkan buku yang dibacanya, makanan tersebut dipercaya dapat mengurangi batuk yang diderita oleh bibinya."},


    {"number" : 6 , "answer": "<img style='max-width:480px;'src=\"data/images/07/soal C revisi.jpg\">"},


    {"number" : 7 , "answer": "<ul><li>(&#10003;)Jarak.</li><li>(&#10003;)Budaya.</li><li>(&#10003;)Biaya</li></ul>"},


    {"number" : 8 , "answer": "Karena ia takut tidak dapat melanjutkan sekolah untuk bekerja agar dapat melunasi utang keluarganya."},


    {"number" : 9 , "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li>Laki-laki tidak boleh melecehkan perempuan baik secara lisan maupun sikap.</li><li>Harga pernikahan yang tinggi.</li></ul>"},


    {"number" : 10, "answer": "<ul><li>(&#10003;)<i>Serba-Serbi Penyakit Pernapasan</i></li><li>(&#10003;)<i>Mengenal TBC dan Dampaknya</i></li><li>(&#10003;)<i>Jangan Sepelekan Batukmu!</i></li></ul>"},


    {"number" : 11, "answer": "Kemungkinan Jawaban:<ul><li>Ia ingin menjadi dokter.</li><li>Ia teringat ibunya yang sakit TBC.</li></ul>"},


    {"number" : 12, "answer": "Mereka secara umum memandang penyakit sebagai gangguan makhluk gaib."},


    {"number" : 13, "answer": "Untuk menegaskan bahwa Mesilina memiliki harapan akan mimpi-mimpinya."},


    {"number" : 14, "answer": "Ia terus berusaha mencari ilmu dengan membaca untuk mewujudkan mimpinya."},


    {"number" : 15, "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li>Penghormatan terhadap perempuan.</li><li>Kebersamaan/gotong royong dalam menanggung beban.</li><li>Kerja keras.</li><li>Ketaatan kepada aturan</li></ul>"},


    {"number" : 16, "answer": "Setelah menikah ia akan menjadi tanggungan suaminya, bukan orang tuanya lagi."},


    {"number" : 17, "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li>Pernikahan usia dini</li><li>Putus sekolah</li></ul>"},


    {"number" : 18, "answer": "Kemungkinan Jawaban:<ul><li>Karena ia ingin menjadi dokter.</li><li>Karena Mesilina ingin menyembuhkan bibinya.</li><li>Karena Mesilina tertarik pada bidang kesehatan/kedokteran</li></ul>"}
,

    {"number" : 19, "answer": "Menjawab “Setuju“ ATAU “Tidak setuju“ disertai minimal satu alasan.<br>Alasan untuk jawaban “Setuju“:<ul><li>Mesilina bisa mengidentifikasi penyakit ibunya dari informasi yang ia dapatkan.</li><li>Pak Adam dan Lisa mengakui bahwa Mesilina anak cerdas.</li></ul>Alasan untuk jawaban “Setuju“:<ul><li>Pak Adam memuji Mesilina anak cerdas hanya untuk menghibur dan memotivasi, belum tentu menggambarkan bahwa Mesilina anak cerdas.</li><li>Kecerdasan Melisina tidak terlihat pada kemampuan akademiknya di sekolah.</li></ul>"},

    {"number" : 20, "answer": "Menjawab minimal satu dari jawaban berikut:<ul><li>Banyak membaca bisa membuat kita tahu banyak hal.</li><li>Pantang menyerah dalam meraih mimpi.</li><li>Kita harus menghargai budaya dan adat</li></ul>"}




];

