var tempCorrect = [
    {"number" : 1 , "answer": "<ul><li>Benar</li><li>Benar<l/i><li>Salah</li><li>Benar</li></ul>"},
    {"number" : 2 , "answer": "Kenangan bencana tsunami sewaktu ia masih balita."},
    {"number" : 3 , "answer": "Bahasa Devayan."},
    {"number" : 4 , "answer": "Agar Umar melindungi keluarganya saat Smong datang."},
    {"number" : 5 , "answer": "Nelayan"},
    {"number" : 6 , "answer": "Umar, Fathimah, dan Aisyah"},
    {"number" : 7 , "answer": "B-A-D-C"},
    {"number" : 8 , "answer": "Agar generasi setelahnya bisa menyelamatkan diri saat bencana itu terjadi lagi"},
    {"number" : 9 , "answer": "Tubrukan dua lempeng benua di Samudera Hindia"},
    {"number" : 10, "answer": "Karena lempeng kontinental bergeser naik 15 meter sehingga permukaan laut mendadak naik dan membentuk gelombang besar."},
    {"number" : 11, "answer": "Menyebutkan minimal satu dari alasan berikut : <ol><li>Untuk menghindari reruntuhan karena gempa.</li><li> Agar tidak terkena reruntuhan bangunan.</li><li> Agar selamat.</li></ol> "},
    {"number" : 12, "answer": " Semakin banyak air yang surut, semakin besar gelombang pasang"},
    {"number" : 13, "answer": "<ul><li>Setuju<br>Agar ia bisa lebih tenang mencari anaknya tanpa perlu mengkhawatirkan keselamatan istrinya</li><li>Tidak setuju<br> Jika mereka mencari bersama-sama mungkin mereka akan lebih cepat menemukan Aisyah</li></ul>"},
    {"number" : 14, "answer": "Karena napasnya masih stabil"},
    {"number" : 15, "answer": "Gempa, air laut surut, dan binatang panik"},
    {"number" : 16, "answer": "<ul><li>Cara terbaik selamat dari smong adalah dengan menghindarinya.</li><li> Kecepatan lari manusia tidak akan bisa mengalahkan kecepatan smong.</li></ul>"},
    {"number" : 17, "answer": "Karena gelombang besar terjadi di dasar dan tengah laut sehingga tidak terlihat dari permukaan"},
    {"number" : 18, "answer": "Ia berhasil memenuhi tanggung jawab dari neneknya."},
    {"number" : 19, "answer": "Banyak penduduk Simeulue tidak menyadari tanda-tanda tsunami sehingga mereka terlambat menyelamatkan diri"},
    {"number" : 20, "answer": "Menjawab 'Ya' disertai minimal satu alasan berikut ini:<ul><li>Karena tidak ada penduduk yang jadi korban tsunami.</li><li>Seluruh penduduk Simeuleu selamat dari tsunami ketika terjadi tsunami pada 2004.</li></ul> "}

];
