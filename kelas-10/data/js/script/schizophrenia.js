var tempCorrect = [

    {"number" : 1 , "answer": "Aku dan Seta."},

    {"number" : 2 , "answer": "Pasien dan dokter."},

    {"number" : 3 , "answer": "Seta sering menggunakan nama-nama sayuran sebagai nama diri"},

    {"number" : 4 , "answer": "Selalu menyerang dokter"},

    {"number" : 5 , "answer": "<ul><li><strong>Bersikap waspada:</strong> <i>tidak tidur berhari-hari</i><li><strong>Penuh rasa curiga:</strong> <i>menyerang pasien lain, tidak mudah percaya</i><li><strong>Religus:</strong> <i>mengaji, solat lima waktu</i></ul>"},

    {"number" : 6 , "answer": "Dia telah berbuat banyak dosa, dia telah banyak menyakiti orang, maka dia setidaknya harus mengimbangi dengan iman agar kapan pun Tuhan memanggilnya, dia telah siap dengan jawaban-jawaban pembelaan yang dapat meringankan kesalah-kesalahannya"},

    {"number" : 7 , "answer": "Tokoh Seta memiliki pekerjaan masa lalu yang membuatnya stres sehingga dia khawatir Seta akan membahayakan dirinya sendiri dan orang lain"},

    {"number" : 8 , "answer": "<ul><li>Tugas yang dirasakan orang sebagai kewajiban untuk melakukannya</li><li>Sesuatu yang harus dilakukan dan tidak boleh diketahui oleh musuh</li></ul>"},

    {"number" : 9, "answer": "Tokoh Aku meyakinkannya bahwa Aku bukanlah musuhnya"},

    {"number" : 10, "answer": "<ul><li>Aku berpura-pura menjadi temannya</li><li>Aku meyakinkannya bahwa Aku bukan musuhnya.</li></ul>"},

    {"number" : 11, "answer": "Dia marah dengan tindakan Seta yang hampir melukainya atau karena Seta telah menyerangnya hanya karena pengakuannya. Tokoh Seta memiliki pemikiran dan halusinasi yang tidak bisa dibedakan dalam dunia nyata."},

    {"number" : 12, "answer": "Dia ingin menanyai perihal laporan yang dibuat Aku"},

    {"number" : 13, "answer": "Tokoh Aku marah"},

    {"number" : 14, "answer": "Tidak. Lalu kataku,<i> ”Baiklah, aku tak akan melakukan wawancara lagi.”</i>"},

    {"number" : 15, "answer": "Tidak. Karena tokoh Aku sudah merencanakan wawancara dengan pasien tanpa sepengetahuan Dokter Kepala dan perawat"},

    {"number" : 16, "answer": "Akan tetap mempertahankan pekerjaannya seperti tokoh Aku; akan berhenti dari pekerjaan"},

    {"number" : 17, "answer": "Ternyata tokoh Aku bukan dokter atau psikiater"},

    {"number" : 18, "answer": "Ya, karena ternyata Aku bukan dokter/psikiater sehingga juga akan membahayakan pasien; Tidak, karena Dokter Kepala seharusnya menyembuhkan tokoh Aku"},

    {"number" : 19, "answer": "Seta dan tokoh Aku, karena mereka ternyata sama-sama pasien rumah sakit jiwa"},

    {"number" : 20, "answer": "Seseorang yang mempunyai pengurangan kemampuan untuk membedakaan kenyataan dan bukan kenyataan. "}



];

