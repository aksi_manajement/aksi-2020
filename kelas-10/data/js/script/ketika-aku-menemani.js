var tempCorrect = [
    {"number" : 1 , "answer": "Karena ia tidak melihat senyum di mata Ari."},

    {"number" : 2 , "answer": "Mengingatkan manusia akan hal-hal penting dalam hidup"},

    {"number" : 3 , "answer": "Karena saat itu Sedih benar-benar tahu bahwa Ari sudah bisa menerimanya"},

    {"number" : 4 , "answer": "Kematian Bagas"},

    {"number" : 5 , "answer": "Ia berusaha bersosialisasi dengan teman-teman barunya."},

    {"number" : 6 , "answer": "Karena sudah terlalu lama menekan perasaan sedih di hatinya"},

    {"number" : 7 , "answer": "<ul><li>(&#10003;)Kesedihan terasa lebih ringan.</li><li>(&#10003;)Membuat hubungan menjadi semakin dekat.</li><li>(&#10003;)Meningkatkan rasa saling percaya.</li></ul>"},

    {"number" : 8 , "answer": "Tepat<br>karena hatinya bisa merelakan istrinya dan bersyukur kepada Tuhan setelah menulis"},

    {"number" : 9 , "answer": "Mendengarkan kata hati"},

    {"number" : 10, "answer": "Karena pengikutnya menganggap tidak pantas laki-laki menangis di depan umum"},

    {"number" : 11, "answer": "Bisa melukai orang di sekitarnya dan juga dirinya sendiri"},

    {"number" : 12, "answer": "<ul><li>(&#10003;)Sulit tidur.</li><li>(&#10003;)Napasnya berat.</li></ul>"},

    {"number" : 13, "answer": "Setuju<br>Karena mereka belum kenal dekat sehingga temannya belum tentu siap menerima cerita sedih. Ari sebaiknya menceritakan hal seperti ini kepada orang-orang terdekatnya."},

    {"number" : 14, "answer": "Memori"},

    {"number" : 15, "answer": "Agar ingat hal-hal yang berharga dalam hidup. "},

    {"number" : 16, "answer": "Mencari orang yang nyaman untuk berbagi dan mengadu dalam doa"},

    {"number" : 17, "answer": "Agar saya bisa melepasnya ketika sudah waktunya untuk pergi dan setelahnya bisa menerima perasaan lain"},

    {"number" : 18, "answer": "Mengenang, membicarakan, dan mendoakannya"},	

    {"number" : 19, "answer": "Setuju, karena nasihatnya diterima dengan baik oleh Ari."},
	
    {"number" : 20, "answer": "Menyebutkan minimal satu dari pesan moral berikut ini:<ul><li>Perasaan sedih ada untuk diterima.</li><li>Seseorang tidak bisa benar-benar bahagia sebelum ia menerima setiap perasaan yang datang, termasuk sedih.</li><li>Perasaan sedih datang untuk mengingatkan hal-hal penting dalam hidup.</li></ul>"}



];
