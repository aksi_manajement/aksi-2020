var tempCorrect = [
    {"number" : 1 , "answer": "Ia ingin mengembara menemukan sesuatu"},

    {"number" : 2 , "answer": "Gadis itu merasa senang.<br><i>Ada pijar yang semakin membara namun meliuk lembut, tak ada angkara, yang ada cinta.</i>"},

    {"number" : 3 , "answer": "<ul><li>(&#10003;)tubuh kurus</li><li>(&#10003;)rambut panjang</li><li>(&#10003;)bola mata hitam</li></ul>"},

    {"number" : 4 , "answer": "Tokoh “aku” merasakan air mata gadis kecil membasahi perutnya."},

    {"number" : 5 , "answer": "Alternatif jawaban:<ul><li>Karena sama-sama menunjukkan penantian yang begitu lama.</li><li>Karena rindu erat kaitannya dengan penantian</li></ul>"},

    {"number" : 6 , "answer": "mencari ibunya"},

    {"number" : 7 , "answer": "Karena ayah gadis kecil menolak untuk menjadi pemimpin meskipun mampu."},

    {"number" : 8 , "answer": "mencari keuntungan dari laut tanpa menjaganya"},

    {"number" : 9 , "answer": "Untuk menunjukkan bahwa masalah sampah plastik di laut sudah sangat parah."},

    {"number" : 10, "answer": "Marah"},

    {"number" : 11, "answer": "jaring dengan lubang yang rapat"},

    {"number" : 12, "answer": "Hewan laut yang tersiksa oleh ulah manusia"},

    {"number" : 13, "answer": "<ul><li>(&#10003;)serakah</li><li>(&#10003;)cuek </li><li>(&#10003;)tidak berperasaan</li></ul>"},

    {"number" : 14, "answer": "Karena ia tidak sependapat dengan tokoh “aku”."},

    {"number" : 15, "answer": "<ul><li>Angin laut terjadi pada pagi hari bertiup ke arah daratan.</li><b>Kalimat:</b> Angin laut selalu membawa pesannya saat pagi datang.<li>Angin darat terjadi pada senja atau malam hari bertiup kearah lautan.</li><b>Kalimat:</b> <i>Hari sudah semakin senja. Saatnya angin darat membawaku pulang ke samudera.</i>"},

    {"number" : 16, "answer": "<i>Lantas, pada siapa kutitipkan diriku?</i>"},

    {"number" : 17, "answer": "Ya<br>Alasan:<ul><li>Puisi menambahkan alur/jalan cerita pada cerita</li><li>Puisi menambah penjelasan gambaran cerita</li><li>Puisi menambah keindahan cerita</li></ul>Tidak<br>Alasan:<ul><li>Cerita tidak akan berubah jika puisi dihapus</li><li>Cerita dan puisi berdiri sendiri-sendiri</li></ul>"},

    {"number" : 18, "answer": "Ya<br>Alasan:<ul><li>Penulis baru mengungkap nama tokoh Laut di akhir cerita</li><li>Penulis menggambarkan Laut sebagai manusia</li></ul>Tidak<br>Alasan:<ul><li>Penulis secara jelas mengungkapkan hubungan tokoh laut dengan hewan-hewan  laut</li></ul>"},

    {"number" : 19, "answer": "Dari cara ia memandang manusia dan ceritanya tentang “teman-temannya”"},

    {"number" : 20, "answer": "Alternatif jawaban:<ul><li>Pentingnya menjaga laut dan isinya</li><li>Pentingnya menjadi pemimpin jika kita mampu</li></ul>"}



];
