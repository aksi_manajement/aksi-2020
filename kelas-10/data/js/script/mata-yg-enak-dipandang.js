var tempCorrect = [
    {"number" : 1 , "answer": "<img src=\"data/images/02/pilihanc.png\">"},
    {"number" : 2 , "answer": "Pengemis dan penuntunnya "},
    {"number" : 3 , "answer": "Tarsa menuruti perintah Mirta untuk memperlambat jalannya."},
    {"number" : 4 , "answer": "Dia selalu diperas oleh Tarsa ketika Tarsa ingin mendapatkan sesuatu."},
    {"number" : 5 , "answer": "<ul><li> Mirta menabrak sepeda yang diparkir melintang </li><li>Mirta tertindih sepeda</li></ul>"},
    {"number" : 6 , "answer": "Menempatkannya di tempat yang panas"},
    {"number" : 7 , "answer": "<ul><li>Mirta boleh mendesis dan mengumpat sengit</li><li>Mirta jengkel dan tidak ingin diperas terus-menerus</li></ul>"},
    {"number" : 8 , "answer": "Segelas es limun diminumnya dengan penuh rasa kemenangan."},
    {"number" : 9 , "answer": "<ul><li>Tarsa tidak memedulikan Mirta yang tertidur atau pingsan</li><li>Mirta berhasil memeras Tarsa untuk mendapatkan es limun.</li></ul>"},
    {"number" : 10, "answer": "Fisik Mirta."},
    {"number" : 11, "answer": "Karena menurut Mirta, orang-orang di kereta utama tidak memberikan uang."},
    {"number" : 12, "answer": "Tarsa menyalahkan Mirta yang menurutnya tidak bisa mengemis."},
    {"number" : 13, "answer": "<table border='2'><th>Kejadian</th><th>Perasaan</th><tr><td>Mirta berdiri di seberang jalan depan stasiun</td><td><strong>Tega</strong></td></tr><tr><td>Mirta terjatuh menabrak sepeda</td><td><strong>Gembira</strong></td></tr><tr><td>Mirta merebahkan badan, melengkung seperti bangkai udang</td><td><strong>Tidak peduli</strong></td></tr><tr><td>Mirta sudah rebah kembali. Tubuhnya menggigil dan terasa sangat panas</td><td><strong>Menyesal</strong></td></tr></table>"},
    {"number" : 14, "answer": "Ya. <br>Contohnya saat Tarsa membiarkan Mirta berada di bawah panas matahari karena Tarsa ingin membeli es limun. "},
    {"number" : 15, "answer": "Suka memanfaatkan orang lain."},
    {"number" : 16, "answer": "Tidak. Karena perbuatan Tarsa tidak pantas dilakukan terhadap orang-orang yang mengalami disabilitas."},
    {"number" : 17, "answer": "Ya, judul “Mata yang Enak Dipandang” sesuai dengan isi cerpen. Ada bagian yang menjelaskan kaitan antara judul dan isi cerpen. Mata yang enak dipandang adalah inti dari cerpen ini. Dalam cerpen, digambarkan bahwa orang-orang yang suka memberi recehan itu memiliki mata yang berbeda dengan orang-orang lain. Mereka menyatakannya dengan ungkapan “mata yang enak dipandang” "},
    {"number" : 18, "answer": "Mirta digambarkan tidak bergerak, entah meninggal atau pingsan."},
    {"number" : 19, "answer": "Ketiga Jawaban Di Bawah Ini Benar : <br><ul><li>Setiap orang harus dihargai karena suka tidak suka mereka pasti memiliki andil dalam kehidupan seseorang.</li><li>Sikap bergantung pada orang lain bisa membawa petaka.</li><li>Hidup miskin itu tidak mudah.</li></ul>"},
    {"number" : 20, "answer": "<ul><li>6 : Mirta ditinggalkan Tarsa di bawah terik matahari. </li><li>7 : Mirta berjalan tanpa bantuan Tarsa.</li><li>5 : Mirta terjatuh karena menabrak sepeda yang diparkir.</li><li>3 : Mirta minta diantar Tarsa ke tempat yang teduh.</li><li>1 : Di tempat teduh, Mirta tertidur atau pingsan.</li><li>2 : Ketika kereta kelas tiga datang, Mirta masih tetap terbaring diam.</li><li>4 : Mirta terjatuh karena menabrak sepeda yang diparkir.</li></ul>"}

];
