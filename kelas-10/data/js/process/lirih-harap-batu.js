$(document).ready(function() {

    $('#question #submit').on('click', function() {
        var tempAnswers = [];
        var error       = 0;

        $('#question .question').each(function(index, el) {
            ansNum = index + 1;

            if(ansNum == 6) //Radio answers
           {
             if($('input[name=question-'+ansNum+']:checked').length > 0){
                    tempAnswers[ansNum] = '{"complete": "' +  $("#" + $('input[name=question-'+ansNum+']:checked')[0].id + "-text").html().trim().replace(/"/g,'\\"')
                      +'"}';
            }
            else {
               tempAnswers[ansNum] = '{ "complete": ""}';
                error++;
              }
             }
            else if(ansNum == 3 || ansNum == 7 || ansNum == 10) // checkbox answers
        {

                  if($('input[name=question-'+ansNum+']:checked').length > 0){
                      var input   = '';
                      var complete   = '';
                      $('input[name=question-'+ansNum+']:checked').each(function(i) {

                          complete += '<li>(&#10003;)'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().trim().replace(/"/g,'\\"') +'</li>';
                      });
                      tempAnswers[ansNum] = '{  "complete": "<ul>' + complete +'</ul>"}';

                        }else {
                            tempAnswers[ansNum] = '{ "complete": ""}';
                            error++;
                            }
            }
            else if(ansNum == 19) //Radio answers + 1 input
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0 && $("#question-"+ansNum).val() != ""){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"') + '<ul><li>'
                        + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')+ '</li></ul>'
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if( ansNum == 1 || ansNum == 2 || ansNum == 4 || ansNum == 5 || ansNum == 8 || ansNum == 9 || ansNum == 11 || ansNum == 12|| ansNum == 13 || ansNum == 14 || ansNum == 15 || ansNum == 16 || ansNum == 18 || ansNum == 17 || ansNum == 20) //1 input answers
            {
                if($("#question-"+ansNum).val() == '') {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')
                    +'"}';
            }


            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);
        });

        if(error != 0) {
            $('#error-num').html(error);
            $('#error-modal').foundation('open');
        } else {
            response = $(this).attr('href');
            window.location.href = response;
        }

        return false;
    });


});
