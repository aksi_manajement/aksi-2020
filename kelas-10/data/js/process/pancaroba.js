$(document).ready(function() {

    $('#question #submit').on('click', function() {
        var tempAnswers = [];
        var error       = 0;

        $('#question .question').each(function(index, el) {
            ansNum = index + 1;

            if(ansNum == 7 || ansNum == 9 ) //Radio answers
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0 ){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if(ansNum == 1 || ansNum == 4) //2 checkbox answers
        {

                  if($('input[name=question-'+ansNum+']:checked').length > 0){
                      var input   = '';
                      var complete   = '';
                      $('input[name=question-'+ansNum+']:checked').each(function(i) {

                          complete += '<li>'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().trim().replace(/"/g,'\\"') +'</li>';
                      });
                      tempAnswers[ansNum] = '{  "complete": "<ul>' + complete +'</ul>"}';

                        }else {
                            tempAnswers[ansNum] = '{ "complete": ""}';
                            error++;
                            }
            }
            else if(ansNum == 3 || ansNum == 8) //Radio answers + 1 input
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0 && $("#question-"+ansNum).val() != ""){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"') + '<ul><li>'
                        + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')+ '</li></ul>'
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if(ansNum == 6) //Radio answers
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0){

                    tempAnswers[ansNum] = '{"complete": "' +  $("#" + $('input[name=question-'+ansNum+']:checked')[0].id + "-text").html().trim().replace(/"/g,'\\"')
                      +'"}';

                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if( ansNum == 2 || ansNum == 5 || ansNum == 10 || ansNum == 11 || ansNum == 12 || ansNum == 13 || ansNum == 14 || ansNum == 16 || ansNum == 18 || ansNum == 17) //1 input answers
            {
                if($("#question-"+ansNum).val() == '') {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')
                    +'"}';
            }
            // else if(ansNum == 8 || ansNum == 11 || ansNum == 12) //2 input answers
            // {
            //     tempAnswers[ansNum] = '{ "complete": "<strong>Isian 1:</strong><br>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '<br>'
            //         + '<br><strong>Isian 2:</strong><br>' + $("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')
            //         +'"}';
            // }
            else if(ansNum == 15) //5 input answers
            {
                if($("#question-"+ansNum+"-1").val() == ''  ||
                    $("#question-"+ansNum+"-2").val() == '' ||
                    $("#question-"+ansNum+"-3").val() == '' ||
                    $("#question-"+ansNum+"-4").val() == '' ||
                    $("#question-"+ansNum+"-5").val() == '') {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "<ul><li>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-5").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li></ul>"}';
            }
            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);
        });

        if(error != 0) {
            $('#error-num').html(error);
            $('#error-modal').foundation('open');
        } else {
            response = $(this).attr('href');
            window.location.href = response;
        }

        return false;
    });


});
