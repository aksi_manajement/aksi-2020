$(document).ready(function() {

    $('#question #submit').on('click', function() {
        var tempAnswers = [];
        var error       = 0;

        $('#question .question').each(function(index, el) {
            ansNum = index + 1;

             if(ansNum == 13) //Radio answers + 1 input
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0 && $("#question-"+ansNum).val() != ""){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"') + '<ul><li>'
                        + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')+ '</li></ul>'
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if(ansNum == 16) //2 checkbox answers
        {
                  if($('input[name=question-'+ansNum+']:checked').length > 0){
                      var input   = '';
                      var complete   = '';
                      $('input[name=question-'+ansNum+']:checked').each(function(i) {
                          complete += '<li>'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().trim().replace(/"/g,'\\"') +'</li>';
                      });
                      tempAnswers[ansNum] = '{  "complete": "<ul>' + complete +'</ul>"}';
                        }else {
                            tempAnswers[ansNum] = '{ "complete": ""}';
                            error++;
                            }
            }


          else if(ansNum == 12 ) //Radio answers


            {
                if($('input[name=question-'+ansNum+']:checked').length > 0 ){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if(ansNum == 1) //4 dropdowns
            {
                if($("#question-"+ansNum+"-1").val() == 'Pilih' ||
                    $("#question-"+ansNum+"-2").val() == 'Pilih' ||
                    $("#question-"+ansNum+"-3").val() == 'Pilih' ||
                    $("#question-"+ansNum+"-4").val() == 'Pilih') {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "<ul><li>A ' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>B '
                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>C '
                    +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>D '
                    +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li></ul>"}';
            }


            else if( ansNum == 2 || ansNum == 3 || ansNum == 4 || ansNum == 5 || ansNum == 6 || ansNum == 8 || ansNum == 9 || ansNum == 10 || ansNum == 11 || ansNum == 14|| ansNum == 15 || ansNum == 16 || ansNum == 17 || ansNum == 18 || ansNum == 19 || ansNum == 20) //1 input answers
            {
                if($("#question-"+ansNum).val() == '') {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')
                    +'"}';
            }
              else if(ansNum == 7) //4 input answers
            {
                if($("#question-"+ansNum+"-1").val() == '' ||
                    $("#question-"+ansNum+"-2").val() == '' ||
                    $("#question-"+ansNum+"-3").val() == '' ||
                    $("#question-"+ansNum+"-4").val() == '') {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '-'
                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '-'
                    +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '-'
                    +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '"}';
            }



            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);
        });

        if(error != 0) {
            $('#error-num').html(error);
            $('#error-modal').foundation('open');
        } else {
            response = $(this).attr('href');
            window.location.href = response;
        }

        return false;
    });


});
