$(document).ready(function() {



    $('#question #submit').on('click', function() {

        var tempAnswers = [];

        var error       = 0;



        $('#question .question').each(function(index, el) {

            ansNum = index + 1;



            if(ansNum == 1 || ansNum == 4 || ansNum == 6 || ansNum == 11 || ansNum == 15) //Radio answers

            {

                if($('input[name=question-'+ansNum+']:checked').length > 0 ){

                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')

                        +'"}';

                } else {

                    tempAnswers[ansNum] = '{ "complete": ""}';

                    error++;

                }

            }

            else if(ansNum == 2 || ansNum == 10 || ansNum == 12 || ansNum == 20) //2 checkbox answers

        {



                  if($('input[name=question-'+ansNum+']:checked').length > 0){

                      var input   = '';

                      var complete   = '';

                      $('input[name=question-'+ansNum+']:checked').each(function(i) {



                          complete += '<li>'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().trim().replace(/"/g,'\\"') +'</li>';

                      });

                      tempAnswers[ansNum] = '{  "complete": "<ul>' + complete +'</ul>"}';



                        }else {

                            tempAnswers[ansNum] = '{ "complete": ""}';

                            error++;

                            }

            }



            else if(ansNum == 3 || ansNum == 5 || ansNum == 7 || ansNum == 8 || ansNum == 9 || ansNum == 13 || ansNum == 14 || ansNum == 16 || ansNum == 19) //1 input answers

            {

                if($("#question-"+ansNum).val() == '') {

                    error++;

                }



                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')

                    +'"}';

            }

              else if(ansNum == 17) //5 input answers

            {

                if($("#question-"+ansNum+"-1").val() == '' ||

                    $("#question-"+ansNum+"-2").val() == '' ||

                    $("#question-"+ansNum+"-3").val() == '' ||

                    $("#question-"+ansNum+"-4").val() == '' ||

                    $("#question-"+ansNum+"-5").val() == '') {

                    error++;

                }



                tempAnswers[ansNum] = '{ "complete": "<ul><li>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'

                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'

                    +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'

                    +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'

                    +$("#question-"+ansNum+"-5").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li></ul>"}';

            }
            else if(ansNum == 18) //6 input answers
            {
                if($("#question-"+ansNum+"-1").val() == '' ||
                    $("#question-"+ansNum+"-2").val() == '' ||
                    $("#question-"+ansNum+"-3").val() == '' ||
                    $("#question-"+ansNum+"-4").val() == ''||
                    $("#question-"+ansNum+"-5").val() == ''||
                    $("#question-"+ansNum+"-6").val() == '') {
                    error++;
                }
                
                tempAnswers[ansNum] = '{ "complete": "<ul><li>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Pak Hardiman memberikan informasi tentang identitas pemilik robekan kain." + '</li><li>'
                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Kelompok Merdeka memutuskan mencari tahu melalui Pak Susanto." + '</li><li>'
                    +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Pak Tugimin membenarkan adanya rumor tentang hantu Pak Sura." + '</li><li>'
                    +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Dimas berhasil merobek kaos sosok berbaju putih setelah mengejarnya." + '</li><li>'
                    +$("#question-"+ansNum+"-5").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Pak Tugimin  mengakui perannya dalam kasus ditemukannya mayat di Jembatan Sura." + '</li><li>'
                    +$("#question-"+ansNum+"-6").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Kelompok Merdeka gagal menemui Pak Hardiman." + '</li></ul>"}';
            }



            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);

        });



        if(error != 0) {

            $('#error-num').html(error);

            $('#error-modal').foundation('open');

        } else {

            response = $(this).attr('href');

            window.location.href = response;

        }



        return false;

    });





});

