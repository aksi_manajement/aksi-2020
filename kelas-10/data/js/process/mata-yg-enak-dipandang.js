$(document).ready(function() {

    $('#question #submit').on('click', function() {
        var tempAnswers = [];
        var error       = 0;

        $('#question .question').each(function(index, el) {
            ansNum = index + 1;

            if(ansNum == 6 || ansNum == 10 || ansNum == 15) //Radio answers
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0 ){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            	else if(ansNum == 1) //Radio answers
             {
               if($('input[name=question-'+ansNum+']:checked').length > 0){                    tempAnswers[ansNum] = '{"complete": "' +  $("#" + $('input[name=question-'+ansNum+']:checked')[0].id + "-text").html().trim().replace(/"/g,'\\"')                      +'"}';
              }
              else {
                 tempAnswers[ansNum] = '{ "complete": ""}';
                  error++;
                }
               }
            else if(ansNum == 5 || ansNum == 9) //2 checkbox answers
        {

                  if($('input[name=question-'+ansNum+']:checked').length > 0){
                      var input   = '';
                      var complete   = '';
                      $('input[name=question-'+ansNum+']:checked').each(function(i) {

                          complete += '<li>'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().trim().replace(/"/g,'\\"') +'</li>';
                      });
                      tempAnswers[ansNum] = '{  "complete": "<ul>' + complete +'</ul>"}';

                        }else {
                            tempAnswers[ansNum] = '{ "complete": ""}';
                            error++;
                            }
            }

            else if( ansNum == 2 || ansNum == 3 || ansNum == 4 || ansNum == 8 || ansNum == 11 || ansNum == 12 || ansNum == 14|| ansNum == 16 || ansNum == 17 || ansNum == 18 || ansNum == 19) //1 input answers
            {
                if($("#question-"+ansNum).val() == '') {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')
                    +'"}';
            }
              else if(ansNum == 20) //7 input answers
            {
                if($("#question-"+ansNum+"-1").val() == '' ||
                    $("#question-"+ansNum+"-2").val() == '' ||
                    $("#question-"+ansNum+"-3").val() == '' ||
                    $("#question-"+ansNum+"-4").val() == '' ||
                    $("#question-"+ansNum+"-5").val() == '' ||
                    $("#question-"+ansNum+"-6").val() == '' ||
                    $("#question-"+ansNum+"-7").val() == '') {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "<ul><li>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-5").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-6").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-7").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li></ul>"}';
            }
            else if(ansNum == 13) //7 input answers
          {
            var dropOne = $("#drop-"+ansNum+"-1").text();
            var dropTwo = $("#drop-"+ansNum+"-2").text();
            var dropThree = $("#drop-"+ansNum+"-3").text();
            var dropFour = $("#drop-"+ansNum+"-4").text();

            var questionDropOne = $("#question-"+ansNum+"-1").val(dropOne);
            var questionDropTwo = $("#question-"+ansNum+"-2").val(dropTwo);
            var questionDropThree = $("#question-"+ansNum+"-3").val(dropThree);
            var questionDropFour = $("#question-"+ansNum+"-4").val(dropFour);

              if( $("#drop-"+ansNum+"-1").text() == '' || $("#drop-"+ansNum+"-2").text() == '' || $("#drop-"+ansNum+"-3").text() == '' || $("#drop-"+ansNum+"-4").text() == '' ) {
                  error++;
              }

              tempAnswers[ansNum] = '{ "complete": "Perasaan : <ul><li>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                  +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                  +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                  +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li></ul>"}';
          }

            else if(ansNum == 7) //2 input answers
            {
                if($("#question-"+ansNum+"-1").val() == '' ||
                    $("#question-"+ansNum+"-2").val() == ''  ) {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "<ul><li>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li></ul>"}';
            }


            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);
        });

        if(error != 0) {
            $('#error-num').html(error);
            $('#error-modal').foundation('open');
        } else {
            response = $(this).attr('href');
            window.location.href = response;
        }

        return false;
    });


});
