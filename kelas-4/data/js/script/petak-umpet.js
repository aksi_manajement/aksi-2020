var tempCorrect = [
    {"number" : 1 , "answer": "selalu sibuk"},
    {"number" : 2 , "answer": "Menyebutkan minimal dua dari hal-hal yang menunjukkan kemarahan Abi berikut ini : <br><ul><li>Memalingkan muka</li><li>Berpura-pura tidak mendengar</li><li>Menolak ketika diajak bermain</li></ul>"},
    {"number" : 3 , "answer": "kecewa"},
    {"number" : 4 , "answer": "<table border='2'><tr><th>Nama</th><th>Kegiatan Kesukaan</th></tr><tr><td>Ayah</td><td>Menonton</td></tr><tr><td>Ibu</td><td>Memasak</td></tr><tr><td>Kak Adis</td><td>Bermain bersama Abi</td></tr><tr><td>Abi</td><td>Sedang menelepon Sahabatnya</td></tr></table>"},
    {"number" : 5 , "answer": "Harus bersembunyi di ruangan yang disukai."},
    {"number" : 6 , "answer": "Kemungkinan jawaban :<br><ul><li>Abi khawatir ia yang akan jaga pertama.</li><li>Tidak menyangka akan ada aturan seperti itu.</li></ul>"},
    {"number" : 7 , "answer": "<ul><li>(&#10003;)Ayah dan Kak Adis menyambut semangat usul Ibu, bahkan Abi yang sedang marah.</li><li>(&#10003;)Semua bekerja sama meredakan kemarahan Abi dengan bermain petak umpet. </li></ul>"},
    {"number" : 8 , "answer": "<ul><li>(&#10003;)Ayah paling menyukai ruang keluarga.</li><li>(&#10003;)Nama kucing mereka adalah Miaw.</li></ul>"},
    {"number" : 9 , "answer": "Menyebutkan minimal dua dari hal-hal yang menunjukkan kreativitas Ibu Abi berikut ini :  <br><ul><li>Memiliki ide petak umpet spesial</li><li>Ibu bisa memasak berbagai macam lauk dan cemilan. </li><li>Bersembunyi di tempat yang tidak biasa atau tidak terduga.</li></ul>"},
    {"number" : 10, "answer": "Menjawab minimal dua dari daftar berikut ini :   <br><ul><li>Donat</li><li>Lemper</li><li>Cakue</li></ul>"},
    {"number" : 11 , "answer": "Karena biasanya Kak Adis selalu menghabiskan waktu di kamarnya."},
    {"number" : 12 , "answer": "Tertidur pulas."}
    ,
    {"number" : 13 , "answer": "kolong tempat tidur Abi adalah ruangan spesial bagi Kak Adis"}
    ,
    {"number" : 14 , "answer": "Tidak tepat, karena sebaiknya Abi mencari tahu terlebih dahulu alasan Kak Adis tidak mau bermain bersamanya."}
    ,
    {"number" : 15 , "answer": "Menjawab minimal satu dari daftar berikut ini :   <br><ul><li>Keluarga/ saudara itu tetap saling menyayangi meski sibuk.</li><li>Keluarga itu harus kompak.</li><li>Jangan cepat marah.</li><li>Belajar mengerti kesibukan orang lain.</li></ul>"}

];
