var tempCorrect = [

    {"number" : 1 , "answer": "Ia tinggal di sebatang pohon bintanggor yang tumbuh miring dan menjorok ke arah lautan"},

    {"number" : 2 , "answer": "Ular itu memangsa anak buaya."},

    {"number" : 3 , "answer": "<ul><li>Semua ikan yang ada di Laut Buru takut kepada si Ular.</li><li>Ular akhirnya menyerah dan kalah dari si Buaya Tembaga.</li></ul>"},

    {"number" : 4 , "answer": "Alasan yang tepat (menyebutkan minimal satu):<ol><li>Buaya itu besar dan baik hati.</li><li>Buaya itu suka membantu serta berbadan besar dan kuat.</li><li>Buaya itu selalu melindungi dan menjagai penduduk di daerahnya.</li></ol>"},

    {"number" : 5 , "answer": "Dengan mengibaskan ekornya yang tajam."},

    {"number" : 6 , "answer": "Tidak, karena pertarungan berlangsung berhari-hari."},

    {"number" : 7 , "answer": "Sebagai tanda/ ucapan terima kasih."},

    {"number" : 8 , "answer": "Menjawab minimal tiga dari nama ikan berikut:<ul><li>Ikan make;</li><li>Ikan parang;</li><li>Ikan pepare;</li><li>Ikan salmaneti;</li></ul>"},

    {"number" : 9 , "answer": "<ul><li>Rakus.</li><li>Tamak.</li><li>Egois.</li></ul>"},

    {"number" : 10, "answer": "Menjawab minimal satu dari jawaban berikut: <ol><li>Ikan-ikan senang ada yang akan membantu mereka mengalahkan ular</li><li>Untuk memberi semangat kepada si Buaya Tembaga</li><li>Agar Buaya Tembaga dapat mengalahkan ular</li></ol>"},

    {"number" : 11, "answer": "Menjawab “Sesuai” atau “Tidak sesuai” disertai alasan yang tepat.<br>Alasan yang tepat untuk jawaban “Sesuai”:<ul><li>Buaya Tembaga tidak mudah mengalahkan si ular dan sempat mengalami kesulitan.</li></ul>Alasan yang tepat untuk jawaban “Tidak sesuai”:<ul><li>Buaya Tembaga dapat mengalahkan ular, sedangkan gambarnya ular yang menang.</li></ul>"}

];

