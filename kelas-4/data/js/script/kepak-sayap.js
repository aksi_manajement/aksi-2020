var tempCorrect = [

    {"number" : 1 , "answer": "Burung elang jawa"},

    {"number" : 2 , "answer": "Rasamala"},

    {"number" : 3 , "answer": "<ul><li>Agar Albi aman dari hewan pemangsa.</li><li>Agar Albi tak dicelakai manusia.</li></ul>"},

    {"number" : 4 , "answer": "Menyebutkan salah satu alasan di bawah ini:<ol><li>Karena sayap Albi belum kuat untuk terbang.</li><li>Karena Albi belum berumur 10 minggu.</li><li>Karena Albi belum siap terbang.</li></ol>"},

    {"number" : 5 , "answer": "Kemungkinan  jawaban:<ol><li>Albi setuju dengan perkataan mamanya.</li><li>Albi lapar, sama seperti yang dikatakan mamanya.</li></ol>"},

    {"number" : 6 , "answer": "<ul><li>Burung elang jawa siap terbang setelah berusia 10 minggu.</li><li>Burung elang dapat melihat jelas mangsa yang berada jauh di bawah.</li></ul>"},

    {"number" : 7 , "answer": "7 minggu"},

    {"number" : 8 , "answer": "Makanan elang jawa (menyebutkan minimal 3 makanan)<ol><li>Tikus kecil</li><li>Kadal</li><li>Tupai</li><li>Ular</li></ol>"},

    {"number" : 9 , "answer": "Pemakan daging, berjambul, penglihatan tajam."},

    {"number" : 10 , "answer": "Menyebutkan minimal satu  karakter Albi di bawah ini disertai satu contoh hal yang telah dilakukan:<ol><li>Pantang menyerah</li><li>Optimis</li><li>Penuh semangat</li><li>Berani.</li></ol>Contoh kalimat yang mendukung jawaban “pantang menyerah” dan “optimis”:<ul><li>“aku pasti bisa terbang seperti Mama”</li><li>“Aku pasti bisa meniru gerakan Mama dan Papa”</li><li>Albi akan terus melatih kemampuan terbangnya setiap hari.</li></ul>Contoh kalimat yang mendukung jawaban “penuh semangat” dan “berani”:<ul><li>Semakin lama, Albi semakin berani</li><li>Albi tak gentar bergerak menjauh 10 meter dari sarangnya</li><li>Albi akan terus melatih kemampuan terbangnya setiap hari.</li></ul>"},

    {"number" : 11 , "answer": "Mengepak, melayang, dan meluncur"},

    {"number" : 12 , "answer": "Setuju.<br>Kalimat yang mendukung:<ul><li>Dari kejauhan, Albi melihat seekor kadal kecil (wacana bagian 3)</li><li>Albi tahu, dari tempat mereka kini berada, Mama bisa melihat hewan buruan di bawah sana dengan mudah (wacana bagian 1)</li></ul>Tidak setuju.<br>Kalimat yang mendukung:<ul><li>Albi kesulitan melihat mangsanya (wacana bagian 3)</li></ul>"},

    {"number" : 13 , "answer": "<ul><li>Kepakan sayap Albi ke bawah akan menekan udara</li><li>Udara akan menekan balik dan mendorong tubuh Albi ke atas</li></ul>"},

    {"number" : 14 , "answer": "Meluncur"},

    {"number" : 15 , "answer": "Ya<br>Menyebutkan salah satu alasan di bawah ini:<ul><li>Mama dan Papa Albi mencarikan hewan buruan untuk dimakan Albi.</li><li>Mama dan Papa Albi bergantian memberi Albi makan.</li><li>Mama dan Papa Albi membuatkan sarang yang nyaman dan aman untuk tempat tinggal Albi.</li><li>Mama memberi Albi semangat untuk terus berlatih.</li></ul>Tidak<br>Menyebutkan salah satu alasan di bawah ini:<ul><li>Mama membiarkan Albi berlatih terbang sendiri.</li><li>Papa Albi tidak mengajari Albi terbang.</li></ul>"},

    {"number" : 16 , "answer": "Kemungkinan jawaban:<ul><li>Albi gugup karena itu pengalaman pertamanya menangkap buruan.</li><li>Albi teralu bersemangat untuk menangkap mangsa pertamanya.</li></ul>"}

];