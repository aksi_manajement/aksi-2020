var tempCorrect = [
    {"number" : 1 , "answer": "Pulau Papua"},
    {"number" : 2 , "answer": "Jawaban merujuk pada kondisi cuaca yang sangat panas.<br>Contoh:<br>Tangan Okto tak henti mengipas-ngipas karena kepanasan.<br>Bagian leher kaus okto basah oleh keringat.<br>Frans ingin menyegarkan tubuhnya karena cuaca panas."},
    {"number" : 3 , "answer": "Demam<br>Kepanasan"},
    {"number" : 4 , "answer": "Memilih setia kawan, karena mereka berkeinginan untuk mengajak Enoumbi pergi berenang di sungai.<br><br>Memilih tidak setia kawan, karena mereka tetap pergi berenang di sungai meski tahu temannya sedang sakit."},
    {"number" : 5 , "answer": "Karena Okto juga kepanasan<br>Karena mereka juga akan mengajak Enoumbi"},
    {"number" : 6 , "answer": "Parasit"},
    {"number" : 7 , "answer": "Menyelam untuk menangkap ikan dengan cara menombak"},
    {"number" : 8 , "answer": "Jubi <br>Kacamata renang"},
    {"number" : 9 , "answer": "Kreatif <br>Setia kawan"},
    {"number" : 10, "answer": "<img src=\"data/images/molo/fishing-spear.jpg\">"},
    {"number" : 11, "answer": "Mengikatkan besi pada <span style='font-style: italic;'>jubi</span>."},
    {"number" : 12, "answer": "Bersemangat untuk pergi berenang dan <span style='font-style: italic;'>molo</span>."},
    {"number" : 13, "answer": "Ikan lele & ikan mujair"},
    {"number" : 14, "answer": "Frans dan Okto menanggalkan kaus sebelum melompat ke sungai untuk menyelam dan berenang"},
    {"number" : 15, "answer": "Agar dapat melihat ikan dengan jelas ketika <span style='font-style: italic;'>molo</span>"},
    {"number" : 16, "answer": "Menjawab setuju dengan alasan menunjukkan rasa kesetiakawanan.<br>Misalnya:<ul><li>Karena mereka peduli dengan temannya yang sedang sakit.</li><li>Karena mereka bersahabat dengan Enoumbi.</li><li>Karena mereka ingin memberi ikan kepada Enoumbi yang sedang sakit.</li></ul>"}
];