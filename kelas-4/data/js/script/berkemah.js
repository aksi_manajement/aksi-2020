var tempCorrect = [

    {"number" : 1 , "answer": "Berkemah"},

    {"number" : 2 , "answer": "Dingin."},

    {"number" : 3 , "answer": "<ul><li>rajin</li><li>mandiri</li></ul>"},

    {"number" : 4 , "answer": "Kemungkinan Jawaban:<ul><li>Karena di hutan gelap tidak ada listrik, baterai senter akan cepat habis jika digunakan terus</li><li>Karena Giri dan orang tuanya akan kesusahan/kerepotan kalau senter yang digunakan habis baterai ketika di hutan</li><li>Karena di hutan tidak ada orang yang berjualan baterai jika baterai senter mereka habis</li></ul>"},

    {"number" : 5 , "answer": "suhu tubuhnya panas dan badannya sakit"},

    {"number" : 6 , "answer": "tiga bentol merah"},

    {"number" : 7 , "answer": "Kemungkinan jawaban:<ul><li>Penuh perhatian</li><li>Sangat menyayangi Giri</li><li>Memanjakan Giri</li><li>Cekatan karena langsung membawa Giri ke dokter</li></ul>"},

    {"number" : 8 , "answer": "<ul><li>Sedih</li><li>Kecewa</li></ul>"},

    {"number" : 9 , "answer": "Makanan dan obat."},

    {"number" : 10, "answer": "Tidak Boleh<br><br>Alasan:<br>Karena cacar air adalah penyakit menular"},

    {"number" : 11 , "answer": "Kemungkinan jawaban:<ul><li>Sedih</li><li>Kecewa</li><li>Galau</li></ul>"},

    {"number" : 12 , "answer": "Dua minggu"},

    {"number" : 13 , "answer": "<ul><li>Jalan-jalan ke luar rumah.</li></ul>"},

    {"number" : 14 , "answer": "Ibu ingin menunjukkan sesuatu kepada Giri."},

    {"number" : 15 , "answer": "<ul><li>Gembira.</li><li>Terkejut.</li></ul>"},

    {"number" : 16 , "answer": "Buku-buku dan mainan."},

    {"number" : 17 , "answer": "Kemungkinan jawaban:<ul>Lebih menyenangkan di rumah karena:<li>Makanan terjamin karena mudah didapat</li><li>Suasana terang karena ada lampu dan listrik</li><li>Giri bisa mengangkut banyak mainan dan buku-bukunya, tidak seperti saat di hutan</li></ul><ul>Lebih menyenangkan di hutan karena:<li>Suasana berbeda dengan di rumah</li><li>Lebih seru di hutan karena bisa banyak bertemu hewan liar</li></ul>"},

    {"number" : 18 , "answer": "<ul><li>2 - Giri ternyata sakit cacar air dan dokter melarang Giri berpergian keluar rumah.</li><li>1 - Giri menyiapkan perlengkapan berkemahnya dengan penuh semangat.</li><li>4 - Ibu memberi Giri kejutan berupa tenda yang dipasang di halaman belakang rumah.</li><li>3 - Giri terus beristirahat di kamar, Ibu selalu membawakan makanan dan obatnya ke kamar.</li><ul>"}

];