var tempCorrect = [
    {"number" : 1 , "answer": "singkong."},
    {"number" : 2 , "answer": "<ul><li>Beras ketan.</li><li>Parutan kelapa.</li><li>Gula.</li></ul>"},
    {"number" : 3 , "answer": "Nasi pulut unti terbuat dari beras ketan, parutan kelapa, dan gula."},
    {"number" : 4 , "answer": "Kakak Chris."},
    {"number" : 5 , "answer": "<ul><li>4. Tambahkan sedikit perasan jeruk nipis.</li><li>2. Tuangkan air panas ke tepung sagu.</li><li>1. Aduk tepung yang mulai mengental dengan sendok kayu.</li><li>3. Campur tepung sagu dengan air dingin.</li></ul>"},
    {"number" : 6 , "answer": "Chris tidak tahu kapan tugas menggiling bumbu selesai."},
    {"number" : 7 , "answer": "<img src=\"data/images/makan-patita/sayur_dan_kelapa.jpg\">"},
    {"number" : 8 , "answer": "Karena akan ada acara makan patita."},
    {"number" : 9 , "answer": "Makan bersama khas masyarakat Maluku"},
    {"number" : 10, "answer": "Siang hari karena matahari sudah tinggi"},
    {"number" : 11, "answer": "Orang bisa saling berbagi makanan.<br>Orang dapat saling bertemu.<br>Orang bisa saling berbincang."}
];
