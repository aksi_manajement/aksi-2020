var tempCorrect = [
    {"number" : 1 , "answer": "kunang-kunang"},
    {"number" : 2 , "answer": "perut"},
    {"number" : 3 , "answer": "memancarkan cahaya berwarna hijau kebiruan"},
    {"number" : 4 , "answer": "Kemungkinan kunci jawaban ciri tempat tinggal Kun: <br><ul><li>ditumbuhi semak dan rerumputan tinggi</li><li>terdapat endapan lumpur</li><li>tidak ada aliran air</li><li>gelap</li></ul>"},
    {"number" : 5 , "answer": "Tokoh kunang-kunang yang disebutkan adalah Kun. Alasannya karena di awal paragraph disebutkan kunang-kunang tersebut memancarkan cahaya terang hijau kebiruan. Warna cahaya itu dimiliki oleh Kun"},
    {"number" : 6 , "answer": "Jawaban boleh dipilih di antara berikut ini:<br><ul><li>Terkejut<br>Kali ini, ia terperosok sangat dalam.</li><li>Sedih<br>Ia tak bisa menemukan makanannya dan ingin segera ke luar dari sana.<br>Ia tak mau kehilangan semua temannya.<li>Cemas<br>Di suatu tempat, seekor kunang-kunang sedang mencari jalan ke luar.<li>Senang<br>Di antara semak rimbun, ia terbang ke satu arah di hadapannya sampai melihat sesuatu yang dikenalinya.<br>Meski tetap berhati-hati, ia terbang lebih cepat menuju ke sana.</li></li></li></ul>"},
    {"number" : 7 , "answer": "Pilihan jawabannya:<br><ul><li>Mereka kehilangan tempat tinggal.</li><li>Mereka mencari tempat tinggal baru.</li><li>Mereka mati.</li></ul>"},
    {"number" : 8 , "answer": "<ul><li>Pada saat cuaca tidak bagus, Kun terbawa arus air hujan dan membuatnya tersesat.</li><li>Kun dan teman-temannya menemukan serbuk makanan kesukaan mereka.</li><li>Bantuan penyerbukan oleh Kun dan temannya menyebabkan banyak rerumputan tumbuh di rawa.</li></ul>"},
    {"number" : 9 , "answer": "<strong>Benar</strong><br>Mereka jadi mudah mendapatkan serbuk sari tanaman tersebut sebagai makanan kesukaan mereka."},
    {"number" : 10, "answer": "<strong>Senang.</strong><br><span style='font-style: italic;'>Hore! Kun dan teman-teman menemukan serbuk sari tanaman rerumputan<span><br><strong>Gembira</strong><br><span style='font-style: italic;'>Mereka semua sangat gembira. Terutama, Kun!</span>"},
    {"number" : 11 , "answer": "Pilihan jawaban:<ul><li>Kun dan teman-temannya hanya ingin berkumpul.</li><li>Kun dan teman-temannya hanya ingin makan bersama.</li></ul>"}
];
