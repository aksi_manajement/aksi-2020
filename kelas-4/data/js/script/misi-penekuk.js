var tempCorrect = [
    {"number" : 1 , "answer": "Urutan yang benar tentang kejadian dalam cerita : <br> 2 -  Cliona memutuskan pergi ke masa lalu dan membeli panekuk untuk Ibu. <br> 5 - Cliona menuangkan madu dan menancapkan stoberi di setiap sudut panekuk. <br> 1 - Sudah beberapa hari Cliona gagal membuat panekuk untuk ibunya. <br> 3 - Cliona tidak menemukan kue panekuk yang ia cari. <br> 4 - Cliona pergi ke rumah Kejora dan membuat kue di sana."},
    {"number" : 2 , "answer": "Panekuk"},
    {"number" : 3 , "answer": "Panekoki adalah nama robot pembuat kue milik Cliona.<br>Cliona membuat panekuk di rumah Kejora."},
    {"number" : 4 , "answer": "Mencari kue di toko kue masa lalu.<br>Membuat panekuk bersama Kejora."},
    {"number" : 5 , "answer": "Kagum ATAU Takjub <br> Bukti kalimat: <br> &quot;Wah, indah!!&quot;"},
    {"number" : 6 , "answer": "Puding<br>Kue cokelat<br>Tart keju"},
    {"number" : 7 , "answer": "Menyebutkan minimal satu alasan dari daftar berikut ini:<ul><li>Karena Ibu Cliona menyukai panekuk buatan sendiri.</li><li>Karena panekuk masa lalu lebih enak.</li><li>Karena waktu yang Cliona miliki hanya sedikit.</li><li>Karena Panekoki tidak berhasil membuat panekuk sesuai harapan Cliona.</li></ul>"},
    {"number" : 8 , "answer": "Menyebutkan minimal satu dari beberapa alasan berikut ini:<ul><li>Karena dia akan segera bisa membuat kue untuk ibunya.</li><li>Karena harapannya (untuk membuat panekuk kesukaan ibu) akan segera terwujud.</li><li>Karena dia yakin akan bisa membawa pulang kue panekuk yang dicarinya.</li></ul>"},
    {"number" : 9 , "answer": "Madu"},
    {"number" : 10, "answer": "Sering berlatih membuat panekuk."},
    {"number" : 11, "answer": "Lembut dan harum"},
    {"number" : 12, "answer": "Dia mendengar rencana dan kegagalan Cliona.<br>Panekuk tidak dijual di toko kue."},
    {"number" : 13, "answer": "Kejora bukan ahli panekuk, ia juga memiliki pengalaman yang sama dengan Cliona waktu mempersiapkan panekuk untuk ulang tahun ibunya."},
    {"number" : 14, "answer": "Alternatif jawaban: <br> a) Pantang menyerah karena : <br> <ul><li>Berulang kali mencoba membuat panekuk</li><li>Pergi ke masa lalu</li><li>Mau belajar membuat panekuk bersama Kejora</li></ul>b) Berani karena berusaha mencari sampai pergi ke masa lalu.<br>c) Perhatian/penyayang karena ingin membuatkan kue untuk ulang tahun ibunya."},
    {"number" : 15, "answer": "Pesan dari cerita (menjawab minimal satu):<ul><li>Pantang menyerah membuahkan hasil</li><li>Membuat hadiah spesial itu butuh usaha/ kerja keras</li><li>Meski berbeda tahun, persahabatan bisa terjalin</li><li>Tolong menolong ketika melihat orang lain kesusahan.</li></ul>"}
];