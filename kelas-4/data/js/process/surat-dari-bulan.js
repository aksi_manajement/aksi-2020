$(document).ready(function() {

    $('#question #submit').on('click', function() {
        var tempAnswers = [];
        var error       = 0;

        $('#question .question').each(function(index, el) {
            ansNum = index + 1;

            if(ansNum == 1 || ansNum == 2 || ansNum == 3 || ansNum == 7 || ansNum == 8 || ansNum == 9 || ansNum == 10 ) //Radio answers
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }

            else if(ansNum == 6 ) //1 input answers

            {

                if($("#question-"+ansNum).val() == '') {

                    error++;

                }



                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')

                    +'"}';

            }


            else if(ansNum == 4 || ansNum == 5 || ansNum == 11) //2 checkbox answers
            {
                  if($('input[name=question-'+ansNum+']:checked').length > 0){
                      var input   = '';
                      var complete   = '';
                      $('input[name=question-'+ansNum+']:checked').each(function(i) {
                          complete += '<li>(&#10003;)'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().trim().replace(/"/g,'\\"') +'</li>';
                      });
                      tempAnswers[ansNum] = '{  "complete": "<ul>' + complete +'</ul>"}';
                        }else {
                            tempAnswers[ansNum] = '{ "complete": ""}';
                            error++;
                            }
            }











            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);
        });

        if(error != 0) {
            $('#error-num').html(error);
            $('#error-modal').foundation('open');
        } else {
            response = $(this).attr('href');
            window.location.href = response;
        }

        return false;
    });


});
