$(document).ready(function() {
    
    $('#question #submit').on('click', function() {
        var tempAnswers = [];
        var error       = 0;
    
        $('#question .question').each(function(index, el) {
            ansNum = index + 1;
        
            if(ansNum == 2 || ansNum == 9) //Radio answers
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if(ansNum == 5 || ansNum == 7 || ansNum == 8 || ansNum == 10 || ansNum == 11 || ansNum == 13 || ansNum == 14 || ansNum == 15) //1 input answers
            {
                if($("#question-"+ansNum).val() == '') {
                    error++;
                }
    
                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')
                    +'"}';
            }
            else if(ansNum == 1) //5 input answers
            {
                if($("#question-"+ansNum+"-1").val() == '' ||
                    $("#question-"+ansNum+"-2").val() == '' ||
                    $("#question-"+ansNum+"-3").val() == '' ||
                    $("#question-"+ansNum+"-4").val() == '' ||
                    $("#question-"+ansNum+"-5").val() == '') {
                    error++;
                }
                
                tempAnswers[ansNum] = '{ "complete": "<ul><li>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'
                    +$("#question-"+ansNum+"-5").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li></ul>"}';
            }
            else if(ansNum == 3 || ansNum == 4 || ansNum == 6 || ansNum == 12) // checkbox
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0){
                        var input   = '';
                        var complete   = '';
                        $('input[name=question-'+ansNum+']:checked').each(function(i) {
                            if(i==0)
                                input += '"part-'+(i+1)+'": "'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().replace(/"/g,'\\"')+'"';
                            else
                                input += ', "part-'+(i+1)+'": "'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().replace(/"/g,'\\"')+'"';

                            complete += $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().replace(/"/g,'\\"') +'<br>';
                        });
                        tempAnswers[ansNum] = '{ '+ input +', "complete": "<ul>' + complete +'</ul>"}';
                      } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
    
            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);
        });
        
        if(error != 0) {
            $('#error-num').html(error);
            $('#error-modal').foundation('open');
        } else {
            response = $(this).attr('href');
            window.location.href = response;
        }
        
        return false;
    });
    
    
});