$(document).ready(function() {

    

    $('#question #submit').on('click', function() {

        var tempAnswers = [];

        var error       = 0;

    

        $('#question .question').each(function(index, el) {

            ansNum = index + 1;

        

            if(ansNum == 2 || ansNum == 5 || ansNum == 9 || ansNum == 14) //Radio answers

            {

                if($('input[name=question-'+ansNum+']:checked').length > 0){

                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')

                        +'"}';

                } else {

                    tempAnswers[ansNum] = '{ "complete": ""}';

                    error++;

                }

            }

            else if(ansNum == 1 || ansNum == 4 || ansNum == 6 || ansNum == 7 || ansNum == 11 || ansNum == 12 || ansNum == 16 || ansNum == 17) //1 input answers

            {

                if($("#question-"+ansNum).val() == '') {

                    error++;

                }

    

                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')

                    +'"}';

            }

            else if(ansNum == 3 || ansNum == 8 || ansNum == 13 || ansNum == 15) //checkbox

            {

                if($('input[name=question-'+ansNum+']:checked').length > 0){

                        var input   = '';

                        var complete   = '';

                        $('input[name=question-'+ansNum+']:checked').each(function(i) {

                            if(i==0)

                                input += '"part-'+(i+1)+'": "'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().replace(/"/g,'\\"')+'"';

                            else

                                input += ', "part-'+(i+1)+'": "'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().replace(/"/g,'\\"')+'"';



                            complete += $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().replace(/"/g,'\\"') +'<br>';

                        });

                        tempAnswers[ansNum] = '{ '+ input +', "complete": "<ul>' + complete +'</ul>"}';

                      } else {

                    tempAnswers[ansNum] = '{ "complete": ""}';

                    error++;

                }

            }

            else if(ansNum == 10) //Radio & 1 input answers

            {

                if($('input[name=question-'+ansNum+']:checked').length > 0 && $("#question-"+ansNum+"-3").val() != ''){

                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')

                        + '<br>' + $("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')+'"}';

                } else {

                    tempAnswers[ansNum] = '{ "complete": ""}';

                    error++;

                }

            }

            else if(ansNum == 18) //4 input answers

            {

                if($("#question-"+ansNum+"-1").val() == '' ||

                    $("#question-"+ansNum+"-2").val() == '' ||

                    $("#question-"+ansNum+"-3").val() == '' ||

                    $("#question-"+ansNum+"-4").val() == '' ) {

                    error++;

                }

                

                tempAnswers[ansNum] = '{ "complete": "<ul><li>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Giri ternyata sakit cacar air dan dokter melarang Giri berpergian keluar rumah." + '</li><li>'

                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Giri menyiapkan perlengkapan berkemahnya dengan penuh semangat." + '</li><li>'

                    +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Ibu memberi Giri kejutan berupa tenda yang dipasang di halaman belakang rumah." +'</li><li>'

                    +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + " - Giri terus beristirahat di kamar, Ibu selalu membawakan makanan dan obatnya ke kamar." + '</li></ul>"}';

            }

    

            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);

        });

        

        if(error != 0) {

            $('#error-num').html(error);

            $('#error-modal').foundation('open');

        } else {

            response = $(this).attr('href');

            window.location.href = response;

        }

        

        return false;

    });

    

    

});