$(document).ready(function() {

    $('#question #submit').on('click', function() {
        var tempAnswers = [];
        var error       = 0;

        $('#question .question').each(function(index, el) {
            ansNum = index + 1;

            if(ansNum == 1 || ansNum == 3 || ansNum == 5 || ansNum == 12 || ansNum == 13 ) //Radio answers
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if(ansNum == 14 ) //Radio answers + 1 input
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0 && $("#question-"+ansNum).val() != ""){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"') + ', '
                        + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')+ '&nbsp;'
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if(ansNum == 6 || ansNum == 11 ||  ansNum == 15) //1 input answers

            {

                if($("#question-"+ansNum).val() == '') {

                    error++;

                }



                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')

                    +'"}';

            }

            else if(ansNum == 2 || ansNum == 9 || ansNum == 10) //2 input answers

            {

                if($("#question-"+ansNum+"-1").val() == '' ||

                    $("#question-"+ansNum+"-2").val() == '' ) {

                    error++;

                }



                tempAnswers[ansNum] = '{ "complete": "<ul><li>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li><li>'

                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</li></ul>"}';

            }

            else if(ansNum == 4) //3 input answers
            {
                if($("#question-"+ansNum+"-1").val() == '' ||
                    $("#question-"+ansNum+"-2").val() == ''||
                        $("#question-"+ansNum+"-3").val() == '' ) {
                    error++;
                }
                tempAnswers[ansNum] = '{ "complete": "<table><tr><th>Nama</th><th>Kegiatan Kesukaan</th></tr><tr><td>Ayah</td><td>Menonton</td></tr><tr><td>Ibu</td><td>' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</td></tr><tr><td>Kak Adis</td><td>'
                    +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</td></tr><tr><td>Abi</td><td>'
                        +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + '</td></tr></table>"}';
            }
            /*else if(ansNum == 8) //4 dropdowns
            {
                if($("#question-"+ansNum+"-1").val() == 'Pilih' ||
                    $("#question-"+ansNum+"-2").val() == 'Pilih' ||
                    $("#question-"+ansNum+"-3").val() == 'Pilih' ||
                    $("#question-"+ansNum+"-4").val() == 'Pilih') {
                    tempAnswers[ansNum] = '{"complete":"<i>Belum ada jawaban/lengkapi jawaban</i>"}';
                    error++;
                }else{
                  tempAnswers[ansNum] = '{ "complete": "<ul><li>Ibu bersembunyi di balik kulkas. (' + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + ')</li><li>Ayah paling menyukai ruang keluarga. ( '
                      +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + ')</li><li>Kak Adis bersembunyi di kamarnya. ( '
                      +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + ')</li><li>Nama kucing mereka adalah Miaw ('
                      +$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + ')</li></ul>"}';
                }

            }*/
            else if(ansNum == 7 || ansNum == 8) //2 checkbox answers
            {
                  if($('input[name=question-'+ansNum+']:checked').length > 0){
                      var input   = '';
                      var complete   = '';
                      $('input[name=question-'+ansNum+']:checked').each(function(i) {
                          complete += '<li>(&#10003;)'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().trim().replace(/"/g,'\\"') +'</li>';
                      });
                      tempAnswers[ansNum] = '{  "complete": "<ul>' + complete +'</ul>"}';
                        }else {
                            tempAnswers[ansNum] = '{ "complete": ""}';
                            error++;
                            }
            }











            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);
        });

        if(error != 0) {
            $('#error-num').html(error);
            $('#error-modal').foundation('open');
        } else {
            response = $(this).attr('href');
            window.location.href = response;
        }

        return false;
    });


});
