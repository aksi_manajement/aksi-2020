var messageState = 0;
function loadMessage(){
	if(messageState==0){
		$("#message").fadeIn("slow");
		messageState = 1;
	}
}
function disableMessage(){
	if(messageState==1){
		$("#message").fadeOut("slow");
		messageState = 0;
	}
}

function centerMessage(){
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#message").height();
	var popupWidth = $("#message").width();

	$("#message").css({
		"position": "absolute",
	});
}

$(document).ready(function(){	
	$("body").off("click",".link");
	$("body").on("click",".link", function(){
		centerMessage();
		loadMessage();
	});
	$("body").off("click","#message.click-a-inside a");
	$("body").on("click","#message.click-a-inside a", function(){
		disableMessage();
	});
});

