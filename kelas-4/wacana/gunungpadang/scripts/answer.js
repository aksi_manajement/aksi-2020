var tempCorrect = [
    {"number" : 1 , "answer": "Mengulik Fakta Sejati Gunung Padang"},
    {"number" : 2 , "answer": "Gunung Padang diperkirakan berukuran dua kali lipat lebih luas Candi Borobudur"},
    {"number" : 3 , "answer": "Karena menurut hasil temuan, situs Gunung Padang memiliki usia yang lebih tua dari Piramida Mesir yang dianggap sebagai peradaban tertua di dunia"},
    {"number" : 4 , "answer": "<ul class='numbered'><li>Tempat pemujaan</li><li>Tempat pertemuan</li></ul>"},
    {"number" : 5 , "answer": "Posisi situs yang mengarah ke Gunung Gede Pangrango"},
    {"number" : 6 , "answer": "Batu columnar joint"},
    {"number" : 7 , "answer": "Beberapa bukti yang mendukung penyataan:<ul class='numbered'><li>Pendiri situs Gunung Padang telah menggunakan perencanaan dan perhitungan yang matang hingga bangunan bisa bertahan lama</li><li>Konstruksinya begitu hebat</li><li>Mereka memilih bebatuan berukuran pas menjadi pasak-pasak yang menancap kokoh hingga bangunan bisa bertahan lama</li></ul>"},
    {"number" : 8 , "answer": "1914"},
    {"number" : 9 , "answer": "Untuk mencegah kerusakan dan kemusnahan Situs Gunung Padang"},
    {"number" : 10, "answer": "Untuk mempermudah melihat perkembangan penelitian tentang Gunung Padang; agar pembaca tahu bahwa situs Gunung Padang sudah mulai diteliti sejak lama"},
    {"number" : 11, "answer": "Jawaban yang dapat diterima:<ul class='numbered'><li>Agar mereka tidak membuang sampah sembarangan</li><li>Agar mereka tidak melakukan aksi corat-coret</li><li>Pesan lain yang berhubungan dengan menjaga kelestarian cagar budaya yang dikunjungi</li></ul>"},
    {"number" : 12, "answer": "Jawaban yang dapat diterima:<ul class='numbered'><li>Dengan pemilihan judul yang menggugah kesadaran pembaca</li><li>Dengan memaparkan fakta kerusakan yang terjadi di lokasi</li></ul>"},
    {"number" : 13, "answer": "Pendapat yang pertama menyatakan bahwa situs Gunung Padang sebagai punden berundak. Pendapat yang kedua menyatakan bahwa situs Gunung Padang memiliki ruang dan Lorong di bawahnya"},
    {"number" : 14, "answer": "Pendapat kedua.<br>Alasan yang dapat diterima berupa alasan yang mengacu pada proses penelitian atau penggalian untuk membuktikan bagian bawah situs."}
];