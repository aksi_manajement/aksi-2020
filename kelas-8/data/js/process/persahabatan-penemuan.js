$(document).ready(function() {
    
    $('#question #submit').on('click', function() {
        var tempAnswers = [];
        var error       = 0;
    
        $('#question .question').each(function(index, el) {
            ansNum = index + 1;
        
            if(ansNum == 100) //Radio answers
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if(ansNum == 2 || ansNum == 4 || ansNum == 5 || ansNum == 6 || ansNum == 7 || ansNum == 8 || ansNum == 9 || ansNum == 10 || ansNum == 12 || ansNum == 14 || ansNum == 15) //1 input answers
            {
                if($("#question-"+ansNum).val() == '') {
                    error++;
                }
    
                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')
                    +'"}';
            }
            else if(ansNum == 1 || ansNum == 3) // checkbox
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0){
                        var input   = '';
                        var complete   = '';
                        $('input[name=question-'+ansNum+']:checked').each(function(i) {
                            if(i==0)
                                input += '"part-'+(i+1)+'": "'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().replace(/"/g,'\\"')+'"';
                            else
                                input += ', "part-'+(i+1)+'": "'+ $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().replace(/"/g,'\\"')+'"';

                            complete += $("#" + $('input[name=question-'+ansNum+']:checked')[i].id).val().replace(/"/g,'\\"') +'<br>';
                        });
                        tempAnswers[ansNum] = '{ '+ input +', "complete": "<ul>' + complete +'</ul>"}';
                      } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if(ansNum == 11) //4 input answers
            {
                if($("#question-"+ansNum+"-1").val() == '' ||
                    $("#question-"+ansNum+"-2").val() == '' ||
                    $("#question-"+ansNum+"-3").val() == '' ||
                    $("#question-"+ansNum+"-4").val() == '' ) {
                    error++;
                }
                
                tempAnswers[ansNum] = '{ "complete": "<ul><li>' + "Saat bertemu pertama kali dengan tokoh Aku, Edison sedang membuat percobaan ke-30. - (" + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + ")"+'</li><li>'
                    + "Sebanyak 22 ilmuwan telah melakukan percobaan bola lampu. - (" +$("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + ")"+'</li><li>'
                    + "Edison berhasil menemukan bola lampu yang dapat menyala selama 1.200 jam. - (" +$("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + ")"+'</li><li>'
                    + "Seluruh Amerika memadamkan lampu dan alat listrik selama 3 menit untuk menghormati wafatnya Edison. - ("+$("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') + ")"+'</li></ul>"}';
            }
            else if(ansNum == 13) //10 input answers
            {
                if($("#question-"+ansNum+"-1").val() == '' ||
                    $("#question-"+ansNum+"-2").val() == '' ||
                    $("#question-"+ansNum+"-3").val() == '' ||
                    $("#question-"+ansNum+"-4").val() == '' ||
                    $("#question-"+ansNum+"-5").val() == '' ||
                    $("#question-"+ansNum+"-6").val() == '' ||
                    $("#question-"+ansNum+"-7").val() == '' ||
                    $("#question-"+ansNum+"-8").val() == '' ||
                    $("#question-"+ansNum+"-9").val() == '' ||
                    $("#question-"+ansNum+"-10").val() == '') {
                    error++;
                }
                
                tempAnswers[ansNum] = '{ "complete": "<table><tr><th>' + "Tahun" +'</th><th>'+"Peristiwa"+'</th></tr><tr><td>1854</td><td>Pertemuan pertama dengan tokoh Aku.</td></tr>'
                    + "<tr><td>" + $("#question-"+ansNum+"-1").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td><td>' + $("#question-"+ansNum+"-2").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td></tr>'
                    + "<tr><td>" + $("#question-"+ansNum+"-3").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td><td>' + $("#question-"+ansNum+"-4").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td></tr>'
                    + "<tr><td>" + $("#question-"+ansNum+"-5").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td><td>' + $("#question-"+ansNum+"-6").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td></tr>'
                    + "<tr><td>" + $("#question-"+ansNum+"-7").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td><td>' + $("#question-"+ansNum+"-8").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td></tr>'
                    + "<tr><td>" + $("#question-"+ansNum+"-9").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td><td>' + $("#question-"+ansNum+"-10").val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>') +'</td></tr>'
                    + '</table>"}';
            }
    
            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);
        });
        
        if(error != 0) {
            $('#error-num').html(error);
            $('#error-modal').foundation('open');
        } else {
            response = $(this).attr('href');
            window.location.href = response;
        }
        
        return false;
    });
    
    
});