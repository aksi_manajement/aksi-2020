$(document).ready(function() {

    $('#question #submit').on('click', function() {
        var tempAnswers = [];
        var error       = 0;

        $('#question .question').each(function(index, el) {
            ansNum = index + 1;

            if(ansNum == 1) //Radio answers
            {
                if($('input[name=question-'+ansNum+']:checked').length > 0){
                    tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"')
                        +'"}';
                } else {
                    tempAnswers[ansNum] = '{ "complete": ""}';
                    error++;
                }
            }
            else if( ansNum == 2 || ansNum == 3 || ansNum == 4 || ansNum == 5 || ansNum == 6 || ansNum == 7 || ansNum == 8 || ansNum == 9 || ansNum == 11 || ansNum == 13 || ansNum == 14 || ansNum == 15) //1 input answers
            {
                if($("#question-"+ansNum).val() == '') {
                    error++;
                }

                tempAnswers[ansNum] = '{ "complete": "' + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')
                    +'"}';
            }

            else if(ansNum == 10 || ansNum == 12) //Radio & 1 input answers
            {
              if($('input[name=question-'+ansNum+']:checked').length > 0 && $("#question-"+ansNum).val() != ""){
                  tempAnswers[ansNum] = '{ "complete": "' +  $('input[name=question-'+ansNum+']:checked').val().replace(/"/g,'\\"') + '<br>Alasan :<ul><li>'
                      + $("#question-"+ansNum).val().replace(/"/g,'\\"').replace(/(?:\r\n|\r|\n)/g, '<br>')+ '</li></ul>'
                      +'"}';
              } else {
                  tempAnswers[ansNum] = '{ "complete": ""}';
                  error++;
              }
            }

            $.session.set("UserAnswer-" + ansNum, tempAnswers[ansNum]);
        });

        if(error != 0) {
            $('#error-num').html(error);
            $('#error-modal').foundation('open');
        } else {
            response = $(this).attr('href');
            window.location.href = response;
        }

        return false;
    });


});
