var tempCorrect = [
    {"number" : 1 , "answer": "Terkejut."},
    {"number" : 2 , "answer": "Kemungkinan jawaban:<ul><li>Tidak roboh/masih berdiri, hanya ada beberapa retakan di dinding.</li></ul>"},
    {"number" : 3 , "answer": "Kemungkinan jawaban:<ul><li>Ibu Bina  adalah guru FISIKA, jadi bisa menerangkan dengan lebih jelas daripada Ayah</li><li>Ibu adalah guru fisika, di dalam mata pelajaran fisika ada pengetahuan tentang gempa.</li></ul>"},
    {"number" : 4 , "answer": "Gempa vulkanik"},
    {"number" : 5 , "answer": "Karena di kota Paman tidak ada gunung berapi."},
    {"number" : 6 , "answer": "Karena karyawisatanya ke museum dan Bina <b>menganggap museum itu membosankan.</b>"},
    {"number" : 7 , "answer": "Kemungkinan jawaban:<ul><li>Tiruan</li><li>Duplikat</li></ul>"},
    {"number" : 8 , "answer": "Karena di museum ada alat peraga yang membuat Bina bisa membayangkannya dan lebih mudah mengerti."},
    {"number" : 9 , "answer": "Kemungkinan jawaban:<ul><li>Negeri yang memiliki banyak sekali gunung berapi aktif</li><li>Negeri yang sering mengalami letusan gunung berapi</li></ul>"},
    {"number" : 10, "answer": "Kemungkinan jawaban:<br>Ya<br>Alasan :<ul><li>karena selain ada beragam jenis batuan bumi, mulai dari yang biasa hingga bebatuan berharga, juga ada meteorit.</li></ul>Tidak<br>Alasan :<ul><li>karena beragam jenis batu bisa ditemukan di mana saja.</li></ul>"},
    {"number" : 11, "answer": "Magma yang panas keluar karena terdorong oleh gas bertekanan tinggi di perut bumi, melewati saluran, dan keluar melalui kawah."},
    {"number" : 12, "answer": "Kemungkinan jawaban:<br>Bisa<br>Alasan :<ul><li>Letusannya hebat, karena kebanyakan benda hangus dan hanya segelintir yang utuh.</li></ul>Tidak bisa<br>Alasan :<ul><li>Barang yang ada terlalu sedikit.</li></ul>"},
    {"number" : 13, "answer": "Kemungkinan jawaban:<ul><li>Senang</li><li>Takjub</li><li>Puas</li></ul>"},
    {"number" : 14, "answer": "Kemungkinan jawaban:<ul><li>Koleksinya banyak</li><li>Ada alat peraga</li><li>Ada simulasi gempa</li></ul>"},
    {"number" : 15, "answer": "Kemungkinan jawaban:<ul><li>Belajar langsung di lapangan</li><li>Praktik/eksperimen langsung, misalnya membuat tiruan gunung berapi</li><li>Mencari di Internet</li><li>Melakukan permainan terkait gempa dan ilmu bumi</li></ul>"}
];
