var tempCorrect = [
    {"number" : 1 , "answer": "<ul><li>penjual <i>naniura</i></li><li>penjual <i>naniura</i> di pasar</li></ul>"},
    {"number" : 2 , "answer": "<ul><li>(&#10003;)Martoba merupakan seorang warga Kampung Hulu.</li><li>(&#10003;)Merendam ikan mas ke dalam air jeruk merupakan hal yang dilakukan Martoba sehingga ikannya tidak berbau amis.</li></ul>"},
    {"number" : 3 , "answer": "<ul><li>(&#10003;)Ukuran ikan mencapai panjang lengan terentang.</li><li>(&#10003;)Ikan itu bersisik rapat, besar-bersar, dan berkilau keemasan.</li></ul>"},
    {"number" : 4 , "answer": "Menyebutkan satu perasaan dari pilihan berikut:<ul><li>Gembira </li><li>Senang</li><li>Bahagia</li><li>Puas</li> </ul>Bukti kalimat:<br>“Woahh! Beruntung! Aku beruntung!” seru Martoba sambil memegang erat pancingnya, “Besar sekali ikanku, seekor saja cukup!” "},
    {"number" : 5 , "answer": "Menyebutkan satu perasaan dari pilihan berikut:<ul><li>Kaget</li><li>Terkejut</li><li>Tidak percaya</li> </ul>Menyebutkan salah satu bukti kalimat dari pilihan berikut:<ul><li>“Heh, kamu siapa? Ikanku, mana ikanku?!” seru Martoba setengah berteriak.</li><li>Matanya terbelalak menyaksikan hal tersebut.</li></ul> "},
    {"number" : 6 , "answer": "Nurung mendapatkan upah"},
    {"number" : 7 , "answer": "Menjawab: Ya<br> Bukti kalimat:<br>Sebelum jadi ikan, Nurung seorang remaja berumur 10 tahun dan sebatang kara.<br>Menjawab: Tidak<br> Bukti kalimat:<br>“Seorang kakek dulu menemaniku, Kak,” ujar Nurung sambil menunduk, “tapi ia meninggal sekitar dua tahun sebelum aku jadi ikan.” "},
    {"number" : 8 , "answer": "<ul><li>(&#10003;)Mengantar barang.</li><li>(&#10003;)Membuat ubi tumbuk.</li><li>(&#10003;)Merapikan rumah.</li></ul>"},
    {"number" : 9 , "answer": "Menjawab satu dari pilihan berikut:<ul><li>sedih</li><li>penuh harap</li><li>berharap</li></ul>"},
    {"number" : 10, "answer": "Ia ingin lebih lama bersama dengan adiknya"},
    {"number" : 11, "answer": "Menjawab satu dari beberapa jawaban berikut:<ul><li>Martoba tidak ingin Nurung kembali menjadi ikan mas.</li><li>Martoba sudah menyayangi Nurung seperti adik kandungnya sendiri.</li><li>Martoba tidak ingin Nurung mengalami kejadian yang dulu lagi.</li></ul>"},
    {"number" : 12, "answer": "Menyebutkan satu dari kalimat berikut:<ul><li>Nurung penasaran ingin menyaksikan kembali upacara tersebut.</li><li>Nurung bermain bersama teman-temannya hingga mereka menyaksikan upacara tersebut di sungai.</li></ul>"},
    {"number" : 13, "answer": "Menyebutkan satu dari jawaban berikut:<ul><li>Hari sudah mulai gelap, adiknya sudah berubah menjadi ikan yang bisa berada di mana saja di dalam sungai tersebut.</li><li>Martoba sudah menduga hal tersebut akan terjadi.</li><li>Martoba ingat kejadian yang pernah diceritakan Nurung ketika mereka pertama kali bertemu.</li></ul>"},
    {"number" : 14, "answer": "Menyebutkan satu dari jawaban berikut:<ul><li>Ia akan menjaga Nurung dengan lebih baik lagi.</li><li>Ia akan mengajak Nurung untuk pindah rumah ke tempat yang jauh dari sungai.</li></ul>"},
    {"number" : 15, "answer": "Menyebutkan satu dari jawaban berikut:<ul><li>Kita harus menuruti perintah orang yang lebih tua.</li><li>Kita harus menepati hal yang sudah kita janjikan kepada orang lain.</li><li>Kita harus dapat menahan diri dari rasa ingin tahu</li>  </ul>"}

];
