var tempCorrect = [
    {"number" : 1 , "answer": "Kacamata, teropong monokuler, lup"},
    {"number" : 2 , "answer": "Mereka meneliti berbagai tanaman"},
    {"number" : 3 , "answer": "<ul><li>sayuran</li><li>buah musiman</li><li>tanaman langka</li><li>tanaman yang berbuah setiap saat</li></ul>"},
    {"number" : 4 , "answer": "Keberadaan Galaksi Bima Sakti di langit"},
    {"number" : 5 , "answer": "Kunang-kunang"},
    {"number" : 6 , "answer": "<ol><li>Pram memakai alat bantu kacamata dan teropong monokuler serta terampil menggunakannya.</li><li>Pram sudah terbiasa berada di alam karena sering melakukannya bersama keluarganya.</li></ul>"},
    {"number" : 7 , "answer": "Merekam petualangan menggunakan kamera video"},
    {"number" : 8 , "answer": "<ul><li>cahaya bintang</li><li>sinar bulan</li><li>lampu senter</li></ul>"},
    {"number" : 9 , "answer": "<ul><li>Kunang-kunang berwarna hijau kebiruan.</li><li>Suara-suara hewan malam.</li><li>Bukit di balik semak.</li><li>Galaksi Bima Sakti.</li></ul>"},
    {"number" : 10, "answer": "<ul><li>Pram demam tinggi saat bayi yang membuat penglihatannya terganggu sehingga hanya bisa melihat jelas dengan jarak pandang terbatas.</li><li>Kacamata tidak membuat penglihatan Pram pulih, melainkan hanya membantu agar semua yang dilihatnya lebih jelas dan tajam.</li><li>Untuk melihat jauh, Pram dibantu oleh teropong, sedangkan untuk melihat jarak dekat dibantu oleh lup.</li></ul>"},
    {"number" : 11, "answer": "Takjub, kaget, terpana, kagum, atau perasaan yang kurang lebih sama."},
    {"number" : 12, "answer": "Supaya dapat dilihat di lain waktu, supaya dapat ditunjukkan kepada orang tua atau teman-temannya, supaya dapat diunggah di media sosial, supaya tidak kehilangan momen berharga ketika berpetualang."},
    {"number" : 13, "answer": "<ul><li>Deburan air terjun.</li><li>Gemericik air sungai.</li><li>Bunyi kodok bersahutan.</li></ul>"},
    {"number" : 14, "answer": "Hari semakin malam, suasana semakin dingin."},
    {"number" : 15, "answer": "Menyebutkan minimal satu dari alasanberikut ini:<ol><li>Teks bercerita tentang petualangan Pram dan Kirana. </li><li>Cerita ini adalah tentang petualangan.</li><li>Tema cerita ini adalah petualangan.</li>Atau jawaban lain yang menunjukkan pemahaman bahwa tema cerita ini adalah petualangan.</ol>"}
];