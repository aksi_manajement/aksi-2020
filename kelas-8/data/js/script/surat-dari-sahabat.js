var tempCorrect = [
    {"number" : 1 , "answer": "Rania"},
    {"number" : 2 , "answer": "Mayang sedang berada di luar negeri"},
    {"number" : 3 , "answer": "<ul><li>(&#10003;)Karena ingin memberi semangat kepada Mayang.</li><li>(&#10003;)Karena ingin berterima kasih kebaikan Mayang.</li><li>(&#10003;)Karena ingin mengingatkan kembali saat mereka bertemu.</li></ul>"},
    {"number" : 4 , "answer": "Rania mengidap distrofi otot yang membuat ototnya melemah dan menyusut sehingga ia tidak bisa berjalan."},
    {"number" : 5 , "answer": "Kemungkinan jawaban:<ul><li>Karena Mayang bersama timnya memenangkan kejuaraan di luar negeri.</li><li>Karena Mayang bersama timnya mendapatkan pelatihan di luar negeri.</li></ul>"},
    {"number" : 6 , "answer": "Karena hanya Rania yang mengirimkan surat kepada Mayang, selain keluarganya. "},
    {"number" : 7 , "answer": "Mengikuti kompetisi dan latihan"},
    {"number" : 8 , "answer": "Kemungkinan jawaban:<ul><li>Bersyukur atas tubuhnya</li><li>Tetap bersemangat di tengah keterbatasan</li></ul>"},
    {"number" : 9 , "answer": "<ul><li>(&#10003;)Mayang pernah tidak percaya diri. dengan tubuhnya.</li><li>(&#10003;)Mayang dan Rania saling menyemangati satu sama lain.</li><li>(&#10003;)Rania memahami sejarah Borobudur.</li></ul>"},
    {"number" : 10, "answer": "Dengan memanfaatkan tubuhnya yang sehat dan kuat."},
    {"number" : 11, "answer": "Mengelilingi searah jarum jam sambil terus naik ke undakan."},
    {"number" : 12, "answer": "Mayang karena ia kesal liburannya akan berakhir."},
    {"number" : 13, "answer": "Kemungkinan jawaban:<ul><li>Rania banyak membaca.</li><li>Rania mencari tahu informasi dari berbagai sumber.</li><li>Rania diberitahu oleh orang lain.</li></ul>"},
    {"number" : 14, "answer": "Kemungkinan jawaban:<br>Setuju:<ul><li>karena Mayang memang seorang atlet angkat besi yang biasa mengangkat beban.</li></ul>Tidak setuju:<ul><li>karena kata-kata itu menyinggung perasaan Rania yang seolah-olah disamakan dengan beban.</li></ul"},
    {"number" : 15, "answer": "Kemungkinan jawaban:<br><b>Tepat, Karena:</b><ul><li>itu adalah bentuk kepedulian terhadap orang lain yang membutuhkan.</li><li>ada orang tua mereka yang mendampingi, jadi tidak akan berbahaya.</li></ul><b>Tidak tepat, karena:</b><ul><li>tindakan itu berisiko, apalagi Mayang tidak begitu mengenal Candi Borobudur. Mereka bisa saja terpeleset dan terjatuh. </li><li>tindakan itu seharusnya dilakukan oleh orang dewasa atau yang lebih kuat dan berpengalaman menaiki Candi Borobudur.</li></ul"}
];
