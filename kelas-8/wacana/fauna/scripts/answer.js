var tempCorrect = [

    {"number" : 1 , "answer": "Yuk Kenalan Sama 4 Fauna Khas Indonesia Ini! | Good News ..."},

    {"number" : 2 , "answer": "(&#10003;) Pesut mahakam <br> (&#10003;) Menggembalakan sapi <br> (&#10003;) Harimau sumatera"},

    {"number" : 3 , "answer": "b. Aktivitas manusia"},

    {"number" : 4 , "answer": "Rakong gading : <i>Dijadikan simbol persatuan</i> <br> Burung cendrawasih : <i>Bulunya dijadikan hiasan pakaian</i>"},

    {"number" : 5 , "answer": "Karena hewan itu merupakan hewan yang dilindungi."},

    {"number" : 6 , "answer": "<table border=3><tr><td><b>Garis Pemisah</b></td><td><b>Wilayah yang dipisahkan</b></td></tr><tr><td>Garis Wallace</td><td><i>Barat</i> dengan <i>Tengah</i></td></tr><tr><td>Garis Weber</td><td><i>Tengah</i> dengan <i>Timur</i></td></tr></table>"},

    {"number" : 7 , "answer": "<table border=3><tr><td><b>Hewan</b></td><td><b>Wilayah</b></td></tr><tr><td>Komodo</td><td><i>Tengah</i></td></tr><tr><td>Babi rusa</td><td><i>Tengah</i></td></tr><tr><td>Orang utan</td><td><i>Barat</i></td></tr><tr><td>Walabi</td><td><i>Timur</i></td></tr></table>"},

    {"number" : 8 , "answer": "Karakteristik hewan"},

    {"number" : 9 , "answer": "Tidak ada hewan yang berkantung, hewan jenis mamalianya besar-besar, terdapat banyak jenis kera dan ikan tawar, serta terdapat sedikit jenis burung berwarana."},

    {"number" : 10, "answer": "<table border=3><tr><td><b>Jenis</b></td><td><b>Tipe Australis</b></td></tr><tr><td>Hewan berkantung</td><td><i>Ada</i></td></tr><tr><td>Hewan mamalia</td><td><i>Kecil</i></td></tr><tr><td>Ikan air tawar</td><td><i>Sedikit</i></td></tr><tr><td>Burung</td><td><i>Banyak yang berwarna dan indah</i></td></tr></table>"},

    {"number" : 11, "answer": "Biawak dan Buaya"},

    {"number" : 12, "answer": "- Indonesia dengan kondisi geografis yang beragam juga menghasilkan fauna yang beragam.<br>- Indonesia memiliki 3 wilayah fauna dengan karakteristik yang beragam"},

    {"number" : 13, "answer": "a. Jumlah spesies yang tersisa"},

    {"number" : 14, "answer": "Terancam"},

    {"number" : 15, "answer": "Perubahan kondisi alam, perburuan liar, alih fungsi habitat, persaingan dengan hewan lain."},

    {"number" : 16, "answer": "(&#10003;) Agar dapat dilakukan upaya pencegahan kepunahan. <br> (&#10003;) Agar diketahui penurunan atau peningkatan jumlah spesies itu."},

    {"number" : 17, "answer": "Badak jawa akan punah saat hewan terakhir itu mati."},

    {"number" : 18, "answer": "Karena hewan langka biasanya hidup di dua wilayah tersebut."},

    {"number" : 19, "answer": "(&#10003;) Sebagai tempat pengembangbiakan hewan. <br> (&#10003;) Sebagai upaya pelestarian hewan langka. <br> (&#10003;) Sebagai pencegahan kepunahan hewan langka."},

    {"number" : 20, "answer": "Melaporkan pemburu tersebut ke pihak berwajib."},

    {"number" : 21, "answer": "<table border=3><tr><td><b>No.</b></td><td><b>Usaha konservasi</b></td><td><b>Pilihan</b></td></tr><tr><td>a</td><td>Memberikan Edukasi kepada masyarakat</td><td>Agar masyarakat setempat ikut menjaga kelestarian hewan</td></tr><tr><td>b</td><td>Membuat Penangkaran</td><td>Hewan langka dapat berkembang biak</td></tr><tr><td>c</td><td>Membuat Papan Larangan</td><td>Mencegah perburuan liar</td></tr><tr><td>d</td><td>Melaporkan Orang yang Berburu Satwa Langka</td><td>Agar pemburu jera memburu lagi</td></tr><tr><td>e</td><td>Menghindari Transaksi Binatang Langka</td><td>Mencegah hewan langka diekspor ke luar habitatnya di Indonesia</td></tr></table>"},

];

